//
//  SZDExtensions.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-16.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import UIKit

extension String
{
    static func prettyString(_ timeInterval: TimeInterval?) -> String?
    {
        guard let timeInterval = timeInterval else
        {
            return nil
        }
        
        let isNegative = timeInterval < 0
        let timeIntervalInMinutes = timeInterval / 60 * (isNegative ? -1 : 1)
        
        let minutes = Int(timeIntervalInMinutes.truncatingRemainder(dividingBy: 60))
        let hours = Int(timeIntervalInMinutes / 60)
        
        let string = (isNegative ? "-" : "") + (hours > 0 ? String(hours) + "h " : "") + String(minutes) + "m"
        
        return string
    }
    
    func height(withWidth width: CGFloat, font: UIFont) -> CGFloat
    {
        let biggestRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingRect = self.boundingRect(with: biggestRect, options: .usesLineFragmentOrigin, attributes: [ NSAttributedString.Key.font : font ], context: nil)
        
        return boundingRect.height
    }
    
    func width(withHeight height: CGFloat, font: UIFont) -> CGFloat
    {
        let biggestRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingRect = self.boundingRect(with: biggestRect, options: .usesLineFragmentOrigin, attributes: [ NSAttributedString.Key.font : font ], context: nil)
        
        return boundingRect.width
    }
}

extension UIImage
{
    public convenience init?(color: UIColor, size: CGSize)
    {
        let rect = CGRect(origin: .zero, size: size)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else {
            return nil
        }
        
        self.init(cgImage: cgImage)
    }
}

extension UIView
{
    func findFirstResponder() -> UIResponder?
    {
        if self.isFirstResponder
        {
            return self
        }
        
        for subview in self.subviews
        {
            if let responder = subview.findFirstResponder()
            {
                return responder
            }
        }
        
        return nil
    }
}

/*extension UIAlertController
{
    
    // Convenience for alert with cancel and single action
    convenience init(title: String, message: String,
                     cancelTitle: String, cancelHandler: ((UIAlertAction) -> Void)?,
                     singleActionTitle: String, singleActionHandler: ((UIAlertAction) -> Void)?,
                     numberOfTextFields: Int)
    {
        self.init(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler)
        let singleAction = UIAlertAction(title: singleActionTitle, style: .default, handler: singleActionHandler)
        
        for _ in 1...numberOfTextFields
        {
            self.addTextField(configurationHandler: nil)
        }
        
        self.addAction(cancelAction)
        self.addAction(singleAction)
    }
}*/
