//
//  SZDDebug.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-02-19.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation

func SZDLog(_ message: String, file: String = #file, function: String = #function, line: Int = #line)
{
    let file = NSURL(fileURLWithPath: file).lastPathComponent ?? file
    
    print(String.init(format: "[SZD][%@:%d %@]: %@", file, line, function, message))
}

func SZDError(_ message: String, file: String = #file, function: String = #function, line: Int = #line)
{
    let file = NSURL(fileURLWithPath: file).lastPathComponent ?? file
    
    print(String.init(format: "[SZD][%@:%d %@]: 📵 ERROR: %@", file, line, function, message))
}
