//
//  SZDColourManager.swift
//  PoopTracker
//
//  Created by Sandy House on 2016-12-25.
//  Copyright © 2016 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class SZDColourManager
{
    static let shared = SZDColourManager()
    
    private let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    private var colours: [SZDColour] = Array()
    public var allColours: [SZDColour]
    {
        get { return self.colours }
    }
    
    // MARK: Initializers 
    
    init()
    {
        self.loadColours()
    }
    
    // MARK: Initialization
    
    private func loadColours()
    {
        SZDLog("Loading colours...")
        
        // fetch or create types
        let fetchRequest: NSFetchRequest = SZDColour.fetchRequest()
        
        // TODO: sort by # of entries
        let sortDescriptor = NSSortDescriptor(key: "index", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            self.colours = try context.fetch(fetchRequest)
        }
        catch {
            SZDError("Failed to fetch SZDColours")
        }
        
        self.initializeColours()
    }
    
    private func initializeColours()
    {
        if self.colours.isEmpty
        {
            self.add(colour: UIColor.init(red: 0, green: 0, blue: 0, alpha: 0), name: "None")
        }
    }
    
    // MARK: Public Management
    
    // MARK: Modifiers
    
    public func add(colour: UIColor, name: String?)
    {
        SZDLog("ADD colour: \(colour) \(name)")
        
        let newColour = self.addFromContext(colour: colour, name: name)
        self.colours.append(newColour)
    }

    public func delete(_ colour: SZDColour)
    {
        SZDLog("DEL colour: \(colour) \(colour.name)")
        
        if let index = self.colours.index(of: colour)
        {
            self.colours.remove(at: index)
        }
        self.deleteFromContext(colour)
    }

    public func update(colour: SZDColour, name: String?)
    {
        colour.update(name: name)
    }
    
    // MARK: Accessors
    
    /// Get colour with colour
    public func colour(with colour: UIColor) -> SZDColour?
    {
        if let colourIndex = self.colours.index(where: { $0.colour == colour })
        {
            return self.colours[colourIndex]
        }
        
        return nil
    }
    
    public func colour(at index: Int) -> SZDColour?
    {
        if index < 0 || index >= self.colours.count
        {
            return nil
        }
        
        return self.colours[index]
    }
    
    // Get colour with name
    public func colour(with name: String) -> SZDColour?
    {
        if let colourIndex = self.colours.index(where: { $0.name == name })
        {
            return self.colours[colourIndex]
        }
        
        return nil
    }
    
    // Get index of colour - colour should exist
    public func index(of colour: SZDColour) -> Int?
    {
        return self.colours.index(of: colour)
    }
    
    // MARK: Private Management
    
    private func addFromContext(colour: UIColor, name: String?) -> SZDColour
    {
        let newColour = NSEntityDescription.insertNewObject(forEntityName: "SZDColour", into: self.context) as! SZDColour
        
        newColour.colour = colour
        newColour.name = (name ?? "").isEmpty ? "Colour \(self.colours.count)" : name!
        newColour.index = Int16(self.colours.count)
        
        return newColour
    }
    
    private func deleteFromContext(_ colour: SZDColour)
    {
        self.context.delete(colour)
    }
    
    /*public func move(colour: SZDColour, to index: Int)
    {
        guard let index = index, index >= 0 && index < self.colours.count else
        {
            SZDError("Out of bounds")
            return
        }
     
        guard let colourIndex = self.index(for: colour) else
        {
            SZDError("No index")
            return
        }
        
        
    }*/
    
    // TODO: should we allow them to change the actual colour?
    /*func editColour(_ colour: SZDColour, name: String)
    {
        SZDLog("Colour name: \(colour.name) -> \(name)")
        colour.name = name
    }*/
    
    // Set the colour for entry using its index 
    // Set the colour for entry using the colour
    
    public func setColourFor(entry: SZDEntry, with index: Int)
    {
        if index >= self.colours.count
        {
            SZDLog("Warning: \(index) is beyond \(self.colours.count)")
            return
        }
        
        let newColour = self.allColours[index]
        
        entry.colour = newColour
    }
    
}
