//
//  SZDFoodManager.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-11.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class SZDFoodManager
{
    static let shared = SZDFoodManager()
    
    private let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    private var foods: [SZDFood] = Array()
    public var allFoods: [SZDFood]
    {
        get { return self.foods }
    }

    // MARK: Initializers
    
    init()
    {
        self.loadFoods()
    }
    
    // MARK: Initialization
    
    private func loadFoods()
    {
        SZDLog("Loading foods...")
        
        // fetch or create types
        let fetchRequest: NSFetchRequest = SZDFood.fetchRequest()
        
        // TODO: sort by # of entries
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            self.foods = try context.fetch(fetchRequest)
        }
        catch {
            SZDError("Failed to fetch SZDFoods")
        }        
    }
    
    // MARK: Public Management
    
    // MARK: Modifiers
    
    public func add(name: String)
    {
        SZDLog("ADD food: \(name)")
        
        if !self.contains(with: name)
        {
            let newfood = self.addFromContext(name: name)
            self.foods.append(newfood)
        }
    }
    
    public func delete(name: String)
    {
        if let food = self.food(with: name)
        {
            self.delete(food)
        }
    }
    
    public func delete(_ food: SZDFood)
    {
        SZDLog("DEL food: \(food) \(food.name)")
        
        if let index = self.index(of: food.name)
        {
            self.foods.remove(at: index)
        }
        
        self.deleteFromContext(food)
    }
    
    public func update(food: SZDFood, name: String)
    {
        food.update(name: name)
    }
    
    // MARK: Accessors
    
    public func food(at index: Int) -> SZDFood?
    {
        if index < 0 || index >= self.foods.count
        {
            return nil
        }
        
        return self.foods[index]
    }
    
    // Get food with name
    public func food(with name: String) -> SZDFood?
    {
        if let index = self.index(of: name)
        {
            return self.foods[index]
        }
        
        return nil
    }
    
    public func index(of name: String) -> Int?
    {
        if let index = self.foods.index(where: { $0.name == name })
        {
            return index
        }
        
        return nil
    }
    
    public func contains(with name: String) -> Bool
    {
        if let _ = self.food(with: name)
        {
            return true
        }
        
        return false
    }
    
    // MARK: Private Management
    
    private func addFromContext(name: String) -> SZDFood
    {
        let newfood = NSEntityDescription.insertNewObject(forEntityName: "SZDFood", into: self.context) as! SZDFood
        
        newfood.name = name
        
        return newfood
    }
    
    private func deleteFromContext(_ food: SZDFood)
    {
        self.context.delete(food)
    }
    
}
