//
//  SZDLocationManager.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-03.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import CoreLocation

class SZDLocationManager: NSObject
{
    static let shared = SZDLocationManager()
    
    private let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    private var locations: [SZDLocation] = Array()
    public var allLocations: [SZDLocation]
    {
        get { return self.locations }
    }
    
    let locationManager = CLLocationManager()
    
    // MARK: Initializers 
    
    override init()
    {
        super.init()
        
        self.loadLocations()
    }
    
    // MARK: Initialization 
    
    private func loadLocations()
    {
        SZDLog("Loading known locations...")
        
        // fetch or create types
        let fetchRequest: NSFetchRequest = SZDLocation.fetchRequest()
        
        // TODO: sort by # of entries
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            self.locations = try context.fetch(fetchRequest)
        }
        catch {
            SZDError("Failed to fetch SZDLocations")
        }
        
        self.initializeLocations()
    }
    
    private func initializeLocations()
    {
        if self.locations.isEmpty
        {
            self.add(location: CLLocation.init(latitude: 90, longitude: 0), name: "Unknown", desc: "No location provided", image: nil)
        }
    }
    
    // MARK: Public Management 
    
    // MARK: Modifiers 
    
    public func add(location: CLLocation, name: String?, desc: String?, image: UIImage?)
    {
        SZDLog("ADD location: \(location.coordinate.longitude) \(location.coordinate.latitude)) \(name)")
        
        let newLocation = self.addFromContext(location: location, name: name, desc: desc, image: image)
        self.locations.append(newLocation)
    }
    
    public func delete(_ location: SZDLocation)
    {
        SZDLog("DEL location: \(location.location.coordinate) \(location.name)")
        
        if let index = self.locations.index(of: location)
        {
            self.locations.remove(at: index)
        }
        self.deleteFromContext(location)
    }
    
    public func update(location: SZDLocation, name: String?)
    {
        location.update(name: name)
    }
    
    // func locationInRange
    // only add if no location in range
    
    // MARK: Accessors
    
    /// Get location with location
    public func knownLocation(with location: CLLocation) -> SZDLocation?
    {
        if let locationIndex = self.locations.index(where: { $0.location == location })
        {
            return self.locations[locationIndex]
            
        }
        else if let knownLocation = self.closestKnownLocation(to: location, maxDistance: 5)
        {
            return knownLocation
        }
        
        return nil
    }
    
    // maxDistance in metres
    public func closestKnownLocation(to location: CLLocation, maxDistance: Int) -> SZDLocation?
    {
        for knownLocation in self.locations
        {
            if (knownLocation.location.distance(from: location) <= Double(maxDistance))
            {
                return knownLocation
            }
        }
        
        return nil
    }
    
    public func knownLocation(at index: Int) -> SZDLocation?
    {
        if index < 0 || index >= self.locations.count
        {
            return nil
        }
        
        return self.locations[index]
    }
    
    public func knownLocation(of name: String) -> SZDLocation?
    {
        if let locationIndex = self.locations.index(where: { $0.name == name })
        {
            return self.locations[locationIndex]
        }
        
        return nil
    }
    
    // Get index of location - location should exist
    public func index(of knownLocation: SZDLocation) -> Int?
    {
        return self.locations.index(of: knownLocation)
    }
    
    // MARK: Private Management
    
    private func addFromContext(location: CLLocation, name: String?, desc: String?, image: UIImage?) -> SZDLocation
    {
        let newLocation = NSEntityDescription.insertNewObject(forEntityName: "SZDLocation", into: self.context) as! SZDLocation
        
        newLocation.location = location
        newLocation.name = (name ?? "").isEmpty ? "\(location.coordinate.latitude) \(location.coordinate.longitude)" : name!
        newLocation.desc = desc
        newLocation.image = image
        
        return newLocation
    }
    
    private func deleteFromContext(_ location: SZDLocation)
    {
        self.context.delete(location)
    }
    
    // MARK: CoreLocationManager
    
    func requestCurrentLocation()
    {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = 5 // m
        
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
}

extension SZDLocationManager: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let currentLocation = locations.last
        {
            self.locationManager.stopUpdatingLocation()
            
            NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "CurrentLocation"), object: nil, userInfo: [ "currentLocation" : currentLocation ])
        }
    }
}
