//
//  SZDCharacteristicManager.swift
//  PoopTracker
//
//  Created by Sandy House on 2016-12-25.
//  Copyright © 2016 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class SZDCharacteristicManager
{
    static let shared = SZDCharacteristicManager()
    
    private let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    private var characteristics: [SZDCharacteristic] = Array()
    public var allCharacteristics: [SZDCharacteristic]
    {
        get { return self.characteristics }
    }
    
    // MARK: Initializers
    
    init()
    {
        self.loadCharacteristics()
    }
    
    // MARK: Initialization
    
    private func loadCharacteristics()
    {
        SZDLog("Loading characteristics...")
        
        // fetch or create types
        let fetchRequest: NSFetchRequest = SZDCharacteristic.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "index", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            self.characteristics = try context.fetch(fetchRequest)
        }
        catch {
            SZDError("Failed to fetch SZDCharacteristics")
        }
        
        // TODO: Remove
        if self.characteristics.isEmpty
        {
            self.add(characteristic: "Amount", desc: nil, icon: nil, ranges: [0, 100])
        }

    }
    
    // MARK: Public Management
    
    // MARK: Modifiers
    
    public func add(characteristic name: String, desc: String?, icon: UIImage?, ranges: [Int16])
    {
        SZDLog("ADD characteristic: \(name)")
        
        let newCharacteristic = self.addFromContext(characteristic: name, desc: desc, icon: icon)
        newCharacteristic.setLevels(ranges: ranges)
        self.characteristics.append(newCharacteristic)
        
        // add the characteristic to entries? 
        //self.addCharacteristicToEntries(characteristic: newCharacteristic)
    }
    
    public func delete(_ characteristic: SZDCharacteristic)
    {
        SZDLog("DEL characteristic: \(characteristic.name)")
        
        if let index = characteristics.index(of: characteristic)
        {
            self.characteristics.remove(at: index)
        }
        self.deleteFromContext(characteristic)
    }
    
    public func update(characteristic: SZDCharacteristic, name: String?, desc: String?, icon: UIImage?, ranges: [Int16]?)
    {
        characteristic.update(name: name, desc: desc, icon: icon, ranges: ranges)
    }
    
    // MARK: Accessors
    
    // TODO: get it according to characteristics ordering
    public func characteristic(at index: Int) -> SZDCharacteristic?
    {
        if let characterIndex = self.characteristics.index(where: { Int($0.index) == index })
        {
            return self.characteristics[characterIndex]
        }
        
        return nil
    }
    
    public func characteristic(with id: String) -> SZDCharacteristic?
    {
        if let characterIndex = self.characteristics.index(where: { $0.id == id })
        {
            return self.characteristics[characterIndex]
        }
        
        return nil
    }
    
    // should exist
    public func index(of characteristic: SZDCharacteristic) -> Int?
    {
        return self.characteristics.index(of: characteristic)
    }
    
    public func move(characteristic: SZDCharacteristic, to index: Int)
    {
        if index < 0 || index >= self.characteristics.count
        {
            SZDError("Out of bounds")
            return
        }
        
        guard let characteristicIndex = self.index(of: characteristic) else 
        {
            SZDError("No index")
            return
        }
        
        self.characteristics.remove(at: characteristicIndex)
        self.characteristics.insert(characteristic, at: index)
    }
    
    // MARK: Levels 
    
    public func level(of characteristic: SZDCharacteristic, for rating: Int16) -> SZDCharacteristicLevel?
    {
        return characteristic.level(for: rating)
    }
    
    // MARK: Private Management 
    
    private func addFromContext(characteristic name: String, desc: String?, icon: UIImage?) -> SZDCharacteristic
    {
        let newCharacteristic = NSEntityDescription.insertNewObject(forEntityName: "SZDCharacteristic", into: self.context) as! SZDCharacteristic
        
        newCharacteristic.name = name
        newCharacteristic.desc = desc
        newCharacteristic.icon = icon
        newCharacteristic.index = Int16(self.characteristics.count)
        newCharacteristic.id = UUID().uuidString
        
        return newCharacteristic
    }
    
    private func deleteFromContext(_ characteristic: SZDCharacteristic)
    {
        self.context.delete(characteristic)
    }
    
    func addCharacteristicToEntries(characteristic: SZDCharacteristic)
    {
        // add characteristic rating
        for entry in SZDPoopManager.shared.allEntries
        {
            entry.characteristics[characteristic.id] = 0
        }
        
        // add entries to default characteristic level 
        if let level = characteristic.levels.object(at: 0) as? SZDCharacteristicLevel, level.min == 0 && level.max == 0
        {
            SZDLog("Adding all to none level")
            level.addToEntries(NSSet(array: SZDPoopManager.shared.allEntries))
        }
    }

}
