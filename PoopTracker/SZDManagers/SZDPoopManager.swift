//
//  SZDPoopManager.swift
//  PoopTracker
//
//  Created by Sandy House on 2016-12-25.
//  Copyright © 2016 sandzapps. All rights reserved.
//

/*
    Goals: 
        - Manage all the poops
        - Ordered by date
 
    Have dictionaries by years, months, days
 
    Gameplan: 
        - Implement entries Array for now 
        - Implement by date later 
        - Make it so adding entries is easy
        - First: add entries with non-relationships + Type
 
 */
import Foundation
import CoreData
import UIKit

class SZDPoopManager
{
    static let shared = SZDPoopManager()
    
    let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    private var entries: [SZDEntry]
    public var allEntries: [SZDEntry]
    {
        get { return self.entries }
    }
    
    // Dictionary of years of months of days of an array of entries
    private var entriesByDate: [Int : [Int : [Int : Array<SZDEntry>]]]
    
    // MARK: Initialization
    
    init()
    {
        self.entries = Array()
        self.entriesByDate = Dictionary()
        
        self.fetchEntries()
    }
    
    /// fetch entries from context in descending order by startDate
    private func fetchEntries()
    {
        let fetchRequest: NSFetchRequest = SZDEntry.fetchRequest()
        let dateSortDescriptor = NSSortDescriptor(key: "startDate", ascending: false) // newest first
        fetchRequest.sortDescriptors = [dateSortDescriptor]
        
        do {
            self.entries = try context.fetch(fetchRequest)
        }
        catch {
            SZDError("Failed to fetch SZDEntrys")
        }
    }
    
    // MARK: Management
    
    // create new entry from context
    func newEntry() -> SZDEntry
    {
        let newEntry = NSEntityDescription.insertNewObject(forEntityName: "SZDEntry", into: self.context) as! SZDEntry
        
        newEntry.poopType = SZDTypeManager.shared.allPoopTypes[0]
        newEntry.colour = SZDColourManager.shared.allColours[0]
        
        // for every characteristic, set its value to 0 
        for characteristic in SZDCharacteristicManager.shared.allCharacteristics
        {
            newEntry.characteristics[characteristic.id] = 0
        }
        
        SZDLog("New Entry: \(newEntry.startDate) \(newEntry.poopType.name) \(newEntry.poopType.entries)")
        
        self.add(entry: newEntry)
        
        return newEntry
    }
    
    func add(entry: SZDEntry)
    {
        
        // TODO: add to relationships
        
        entries.append(entry)
    }
    
    func remove(entry: SZDEntry)
    {
        if let index = entries.index(of: entry)
        {
            entries.remove(at: index)
        }
        self.context.delete(entry)
    }
    
    func refresh(entry: SZDEntry, mergeChanges: Bool)
    {
        entry.managedObjectContext?.refresh(entry, mergeChanges: false)
    }
    
}
