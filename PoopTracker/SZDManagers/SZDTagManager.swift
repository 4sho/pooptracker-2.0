//
//  SZDTagManager.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-11.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class SZDTagManager
{
    static let shared = SZDTagManager()
    
    private let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    private var tags: [SZDTag] = Array()
    public var allTags: [SZDTag] {
        get { return self.tags }
    }
    
    // MARK: Initializers
    
    init()
    {
        self.loadTags()
    }
    
    // MARK: Initialization
    
    private func loadTags()
    {
        SZDLog("Loading tags...")
        
        // fetch or create types
        let fetchRequest: NSFetchRequest = SZDTag.fetchRequest()
        
        // TODO: sort by # of entries
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            self.tags = try context.fetch(fetchRequest)
        }
        catch {
            SZDError("Failed to fetch SZDTags")
        }
    }
    
    // MARK: Public Management
    
    // MARK: Modifiers
    
    public func add(name: String)
    {
        SZDLog("ADD tag: \(name)")
        
        if !self.contains(with: name)
        {
            let newtag = self.addFromContext(name: name)
            self.tags.append(newtag)
        }
    }
    
    public func delete(name: String)
    {
        if let tag = self.tag(with: name)
        {
            self.delete(tag)
        }
    }
    
    public func delete(_ tag: SZDTag)
    {
        SZDLog("DEL tag: \(tag) \(tag.name)")
        
        if let index = self.index(of: tag.name)
        {
            self.tags.remove(at: index)
        }
        
        self.deleteFromContext(tag)
    }
    
    public func update(tag: SZDTag, name: String)
    {
        tag.update(name: name)
    }
    
    // MARK: Accessors
    
    public func tag(at index: Int) -> SZDTag?
    {
        if index < 0 || index >= self.tags.count
        {
            return nil
        }
        
        return self.tags[index]
    }
    
    // Get tag with name
    public func tag(with name: String) -> SZDTag?
    {
        if let index = self.index(of: name)
        {
            return self.tags[index]
        }
        
        return nil
    }
    
    public func index(of name: String) -> Int?
    {
        if let index = self.tags.index(where: { $0.name == name })
        {
            return index
        }
        
        return nil
    }
    
    public func contains(with name: String) -> Bool
    {
        if let _ = self.tag(with: name)
        {
            return true
        }
        
        return false
    }
    
    // MARK: Private Management
    
    private func addFromContext(name: String) -> SZDTag
    {
        let newtag = NSEntityDescription.insertNewObject(forEntityName: "SZDTag", into: self.context) as! SZDTag
        
        newtag.name = name
        
        return newtag
    }
    
    private func deleteFromContext(_ tag: SZDTag)
    {
        self.context.delete(tag)
    }
    
}
