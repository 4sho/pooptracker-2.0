//
//  SZDTypeManager.swift
//  PoopTracker
//
//  Created by Sandy House on 2016-12-25.
//  Copyright © 2016 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class SZDTypeManager
{
    static let shared = SZDTypeManager()
    
    private let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    private var poopTypes: [SZDPoopType]
    public var allPoopTypes: [SZDPoopType]
    {
        get { return self.poopTypes }
    }
    
    init()
    {
        self.poopTypes = Array()
        self.loadTypes()
    }
    
    /// Load types from ManagedObjectContext
    private func loadTypes()
    {
        SZDLog("Loading types...")
        
        // fetch or create types 
        let fetchRequest: NSFetchRequest = SZDPoopType.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            self.poopTypes = try context.fetch(fetchRequest)
        }
        catch {
            SZDError("Failed to fetch SZDPoopTypes")
        }
        
        self.initializeTypes()
    }
    
    /// Initialize types if they don't already exist
    private func initializeTypes()
    {
        // TODO: should check if all objects have exact values, if not, should modify
        var missingTypes: [String] = ["None", "Type 1", "Type 2", "Type 3", "Type 4", "Type 5", "Type 6", "Type 7"]
        
        for type in self.poopTypes
        {
            if let name = type.name
            {
                if let index = missingTypes.index(of: name)
                {
                    missingTypes.remove(at: index)
                }
            }
        }
        
        SZDLog("missingTypes: \(missingTypes), types: \(self.poopTypes)")
        
        // add missing types
        for type in missingTypes
        {
            self.poopTypes.append(self.newType(name: type, desc: nil, icon: nil))
        }
    }
    
    /// initialize new type from context and return the result
    private func newType(name: String?, desc: String?, icon: UIImage?) -> SZDPoopType
    {
        let type = NSEntityDescription.insertNewObject(forEntityName: "SZDPoopType", into: self.context) as! SZDPoopType
        
        type.name = name
        type.desc = desc
        type.icon = icon
        
        return type
    }
    
    // MARK: management 
    
    public func setTypeFor(entry: SZDEntry, withIndex index:Int)
    {
        if index >= self.poopTypes.count
        {
            SZDLog("Warning: \(index) is beyond \(self.poopTypes.count)")
            return
        }
        
        let newType = self.allPoopTypes[index]
        
        entry.poopType = newType
    }
    
    public func index(of poopType: SZDPoopType) -> Int?
    {
        return self.poopTypes.index(of: poopType)
    }
    
    public func typeIndexFor(entry: SZDEntry) -> Int
    {
        return self.poopTypes.index(of: entry.poopType)!
    }
    
    public func poopType(at index: Int) -> SZDPoopType?
    {
        if index < 0 || index >= self.poopTypes.count
        {
            return nil
        }
        
        return self.poopTypes[index]
    }
    
}
