//
//  SZDLocation+CoreDataProperties.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-02-19.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import CoreLocation


extension SZDLocation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SZDLocation> {
        return NSFetchRequest<SZDLocation>(entityName: "SZDLocation");
    }

    @NSManaged public var location: CLLocation
    @NSManaged public var name: String?
    @NSManaged public var desc: String?
    @NSManaged public var image: UIImage?
    @NSManaged public var rating: Int16
    @NSManaged public var entries: NSSet?

}

// MARK: Generated accessors for entries
extension SZDLocation {

    @objc(addEntriesObject:)
    @NSManaged public func addToEntries(_ value: SZDEntry)

    @objc(removeEntriesObject:)
    @NSManaged public func removeFromEntries(_ value: SZDEntry)

    @objc(addEntries:)
    @NSManaged public func addToEntries(_ values: NSSet)

    @objc(removeEntries:)
    @NSManaged public func removeFromEntries(_ values: NSSet)

}
