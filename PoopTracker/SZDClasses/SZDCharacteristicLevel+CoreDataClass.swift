//
//  SZDCharacteristicLevel+CoreDataClass.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-12.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit


public class SZDCharacteristicLevel: NSManagedObject/*, NSCoding*/
{

    // coding keys
    let descKey = "SZDCharacteristicLeveldescKey"
    let iconKey = "SZDCharacteristicLeveliconKey"
    let maxKey = "SZDCharacteristicLevelmaxKey"
    let minKey = "SZDCharacteristicLevelminKey"
    let nameKey = "SZDCharacteristicLevelnameKey"
    let characteristicKey = "SZDCharacteristicLevelcharacteristicKey"
    let entriesKey = "SZDCharacteristicLevelentriesKey"
    
    /*override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?)
    {
        super.init(entity: entity, insertInto: context)
    }
    
    public func encode(with aCoder: NSCoder)
    {
        SZDLog("\(self.desc) \(self.icon) \(self.max) \(self.min) \(self.name) \(self.characteristic) \(self.entries)")
        aCoder.encode(self.desc, forKey: self.descKey)
        aCoder.encode(self.icon, forKey: self.iconKey)
        aCoder.encode(self.max, forKey: self.maxKey)
        aCoder.encode(self.min, forKey: self.minKey)
        aCoder.encode(self.name, forKey: self.nameKey)
        aCoder.encode(self.characteristic, forKey: self.characteristicKey)
        aCoder.encode(self.entries, forKey: self.entriesKey)
    }
    
    public required init?(coder aDecoder: NSCoder)
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        guard let entity = NSEntityDescription.entity(forEntityName: "SZDCharacteristicLevel", in: context) else {
            SZDError("NSEntityDescription failed to init.")
            return nil
        }
        
        super.init(entity: entity, insertInto: nil)
        
        /*guard let max = aDecoder.decodeInteger(forKey: self.maxKey) as? Int,
            let min = aDecoder.decodeInteger(forKey: self.minKey) as? Int else
        {
            SZDError("Unable to decode object(s).")
            return nil
        }
        */
        
        let max = aDecoder.decodeObject(forKey: self.maxKey)
        let min = aDecoder.decodeObject(forKey: self.minKey)
        
        /*guard let max = aDecoder.decodeObject(forKey: self.maxKey) as? Int16,
        let min = aDecoder.decodeObject(forKey: self.minKey) as? Int16 else
        {
            SZDError("Unable to decode object(s).")
            return nil
        }*/
        //let max = aDecoder.decodeInteger(forKey: self.maxKey)
        //let min = aDecoder.decodeInteger(forKey: self.minKey)
        
        let desc = aDecoder.decodeObject(forKey: self.descKey) as? String
        let icon = aDecoder.decodeObject(forKey: self.iconKey) as? UIImage
        let name = aDecoder.decodeObject(forKey: self.nameKey) as? String
        //let characteristic = aDecoder.decodeObject(forKey: self.characteristicKey) as? SZDCharacteristic
        //let entries = aDecoder.decodeObject(forKey: self.entriesKey) as? NSSet
        
        
        self.desc = desc
        self.icon = icon
        //self.min = Int16(min)
        //self.max = Int16(max)
        self.name = name
        //self.characteristic = characteristic
        //self.entries = entries
    }*/
}
