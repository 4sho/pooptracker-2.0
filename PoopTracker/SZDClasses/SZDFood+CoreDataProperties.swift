//
//  SZDFood+CoreDataProperties.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-02-19.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData


extension SZDFood {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SZDFood> {
        return NSFetchRequest<SZDFood>(entityName: "SZDFood");
    }

    @NSManaged public var name: String
    @NSManaged public var entries: NSSet?

}

// MARK: Generated accessors for entries
extension SZDFood {

    @objc(addEntriesObject:)
    @NSManaged public func addToEntries(_ value: SZDEntry)

    @objc(removeEntriesObject:)
    @NSManaged public func removeFromEntries(_ value: SZDEntry)

    @objc(addEntries:)
    @NSManaged public func addToEntries(_ values: NSSet)

    @objc(removeEntries:)
    @NSManaged public func removeFromEntries(_ values: NSSet)

}
