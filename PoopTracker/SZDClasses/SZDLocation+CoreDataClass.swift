//
//  SZDLocation+CoreDataClass.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-02-19.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData


public class SZDLocation: NSManagedObject
{
    public func update(name: String?)
    {
        if let name = name
        {
            self.name = name
        }
    }
    
}
