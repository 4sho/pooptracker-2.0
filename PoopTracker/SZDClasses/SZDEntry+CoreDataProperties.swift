//
//  SZDEntry+CoreDataProperties.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-18.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData


extension SZDEntry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SZDEntry> {
        return NSFetchRequest<SZDEntry>(entityName: "SZDEntry");
    }

    @NSManaged public var endDate: Date?
    @NSManaged public var squares: Int16
    @NSManaged public var startDate: Date
    @NSManaged public var wipes: Int16
    @NSManaged public var characteristics: [String : Int16] // TODO: change to Int
    @NSManaged public var colour: SZDColour
    @NSManaged public var foods: NSSet?
    @NSManaged public var location: SZDLocation
    @NSManaged public var poopType: SZDPoopType
    @NSManaged public var tags: NSSet?
    @NSManaged public var comments: String?

}

// MARK: Generated accessors for foods
extension SZDEntry {

    @objc(addFoodsObject:)
    @NSManaged public func addToFoods(_ value: SZDFood)

    @objc(removeFoodsObject:)
    @NSManaged public func removeFromFoods(_ value: SZDFood)

    @objc(addFoods:)
    @NSManaged public func addToFoods(_ values: NSSet)

    @objc(removeFoods:)
    @NSManaged public func removeFromFoods(_ values: NSSet)

}

// MARK: Generated accessors for tags
extension SZDEntry {

    @objc(addTagsObject:)
    @NSManaged public func addToTags(_ value: SZDTag)

    @objc(removeTagsObject:)
    @NSManaged public func removeFromTags(_ value: SZDTag)

    @objc(addTags:)
    @NSManaged public func addToTags(_ values: NSSet)

    @objc(removeTags:)
    @NSManaged public func removeFromTags(_ values: NSSet)

}
