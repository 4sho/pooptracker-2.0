//
//  SZDCharacteristic+CoreDataClass.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-02-19.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class SZDCharacteristic: NSManagedObject, NSCoding
{
    // coding keys
    let descKey = "SZDCharacteristicdescKey"
    let iconKey = "SZDCharacteristiciconKey"
    let nameKey = "SZDCharacteristicnameKey"
    let indexKey = "SZDCharacteristicindexKey"
    let idKey = "SZDCharacteristicidKey"
    let levelsKey = "SZDCharacteristiclevelsKey"
    
    private let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    let noneUpperBound: Int16 = 0
    let maxUpperBound: Int16 = 100
    
    let minLevels = 2 // [0,0] and [1, 100]
    let maxLevels = 6 // [0,0] and 5 levels
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?)
    {
        super.init(entity: entity, insertInto: context)
    }
    
    public func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.desc, forKey: self.descKey)
        aCoder.encode(self.icon, forKey: self.iconKey)
        aCoder.encode(self.name, forKey: self.nameKey)
        aCoder.encode(self.index, forKey: self.indexKey)
        aCoder.encode(self.id, forKey: self.idKey)
        aCoder.encode(self.levels, forKey: self.levelsKey)
    }
    
    public required init?(coder aDecoder: NSCoder)
    {
        SZDLog("INIT CODER:")
        let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        guard let entity = NSEntityDescription.entity(forEntityName: "SZDCharacteristic", in: context) else {
            SZDError("NSEntityDescription failed to init.")
            return nil
        }
        
        super.init(entity: entity, insertInto: nil)
    }
    
    /*public required init?(coder aDecoder: NSCoder)
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        guard let entity = NSEntityDescription.entity(forEntityName: "SZDCharacteristic", in: context) else {
            SZDError("NSEntityDescription failed to init.")
            return nil
        }
        
        super.init(entity: entity, insertInto: nil)
        
        //let name = aDecoder.decodeObject(forKey: self.nameKey)
        //let levels = aDecoder.decodeObject(forKey: self.levelsKey)
        
        guard let name = aDecoder.decodeObject(forKey: self.nameKey) as? String,
            let levels = aDecoder.decodeObject(forKey: self.levelsKey) as? NSOrderedSet else
        {
            SZDError("Unable to decode object(s).")
            return nil
        }
        
        let desc = aDecoder.decodeObject(forKey: self.descKey) as? String
        let icon = aDecoder.decodeObject(forKey: self.iconKey) as? UIImage
        let index = aDecoder.decodeObject(forKey: self.indexKey) as? Int ?? aDecoder.decodeInteger(forKey: self.indexKey)
        
        
        self.desc = desc
        self.icon = icon
        self.name = name
        self.index = Int16(index)
        self.levels = levels
    }*/
 
    func update(name: String?, desc: String?, icon: UIImage?, ranges: [Int16]?)
    {
        if let name = name
        {
            self.name = name
        }
        
        if let desc = desc
        {
            self.desc = desc
        }
        
        if let icon = icon
        {
            self.icon = icon
        }
        
        if let ranges = ranges
        {
            self.setLevels(ranges: ranges)
        }
        
        // if ranges change, must re-organize its entries
    }
    
    // split levels based on Ints. Each Int is the upper inclusive bound
    func setLevels(ranges: [Int16])
    {
        var ranges = ranges
        
        self.removeFromLevels(self.levels)
        
        var lowerBound = noneUpperBound
        
        if ranges[0] != 0
        {
            ranges.insert(0, at: 0)
        }
        
        if ranges[ranges.count - 1] < maxUpperBound
        {
            ranges.append(maxUpperBound)
        }

        // TODO: error handling
        if ranges.count < minLevels || ranges.count > maxLevels
        {
            SZDError("Not enough ranges")
        }
        
        for upperBound in ranges
        {
            if upperBound < lowerBound
            {
                SZDError("upperBound < lowerBound")
            }
            else if maxUpperBound < upperBound
            {
                SZDError("100 < upperBound")
            }
            
            let newLevel = self.newLevel(min: lowerBound, max: upperBound)
            newLevel.characteristic = self
            
            lowerBound = upperBound + 1
        }
    }
    
    func newLevel(min: Int16, max: Int16) -> SZDCharacteristicLevel
    {
        SZDLog("New characteristic level \(min) \(max)")
        
        let newLevel = NSEntityDescription.insertNewObject(forEntityName: "SZDCharacteristicLevel", into: self.context) as! SZDCharacteristicLevel
        
        newLevel.min = min
        newLevel.max = max
        
        return newLevel
    }
    
    func level(for rating: Int16) -> SZDCharacteristicLevel?
    {
        let rating = rating < noneUpperBound ? noneUpperBound : (rating > maxUpperBound ? maxUpperBound : rating)
        
        for level in self.levels
        {
            guard let level = level as? SZDCharacteristicLevel else
            {
                SZDError("Not a SZDCharacteristicLevel")
                continue
            }
            
            if level.min <= rating && rating <= level.max
            {
                return level
            }
        }
        
        return nil
    }
    
}
