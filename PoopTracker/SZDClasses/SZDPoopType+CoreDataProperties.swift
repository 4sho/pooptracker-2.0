//
//  SZDPoopType+CoreDataProperties.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-02-19.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit


extension SZDPoopType {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SZDPoopType> {
        return NSFetchRequest<SZDPoopType>(entityName: "SZDPoopType");
    }

    @NSManaged public var desc: String?
    @NSManaged public var icon: UIImage?
    @NSManaged public var name: String?
    @NSManaged public var entries: NSSet?

}

// MARK: Generated accessors for entries
extension SZDPoopType {

    @objc(addEntriesObject:)
    @NSManaged public func addToEntries(_ value: SZDEntry)

    @objc(removeEntriesObject:)
    @NSManaged public func removeFromEntries(_ value: SZDEntry)

    @objc(addEntries:)
    @NSManaged public func addToEntries(_ values: NSSet)

    @objc(removeEntries:)
    @NSManaged public func removeFromEntries(_ values: NSSet)

}
