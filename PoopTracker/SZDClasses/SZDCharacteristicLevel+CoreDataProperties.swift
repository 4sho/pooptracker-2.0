//
//  SZDCharacteristicLevel+CoreDataProperties.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-20.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData


extension SZDCharacteristicLevel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SZDCharacteristicLevel> {
        return NSFetchRequest<SZDCharacteristicLevel>(entityName: "SZDCharacteristicLevel");
    }

    @NSManaged public var desc: String?
    @NSManaged public var icon: NSObject?
    @NSManaged public var max: Int16
    @NSManaged public var min: Int16
    @NSManaged public var name: String?
    @NSManaged public var characteristic: SZDCharacteristic?
    @NSManaged public var entries: NSSet?

}

// MARK: Generated accessors for entries
extension SZDCharacteristicLevel {

    @objc(addEntriesObject:)
    @NSManaged public func addToEntries(_ value: SZDEntry)

    @objc(removeEntriesObject:)
    @NSManaged public func removeFromEntries(_ value: SZDEntry)

    @objc(addEntries:)
    @NSManaged public func addToEntries(_ values: NSSet)

    @objc(removeEntries:)
    @NSManaged public func removeFromEntries(_ values: NSSet)

}
