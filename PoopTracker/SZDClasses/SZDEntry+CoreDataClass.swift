//
//  SZDEntry+CoreDataClass.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-02-16.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit


public class SZDEntry: NSManagedObject, NSCoding
{
    private let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext

    // TODO: Make these static
    let endDateKey = "endDateKey"
    let squaresKey = "SZDEntrysquaresKey"
    let startDateKey = "SZDEntrystartDateKey"
    let wipesKey = "SZDEntrywipesKey"
    let characteristicsKey = "SZDEntrycharacteristicsKey"
    let colourKey = "SZDEntrycolourKey"
    let foodsKey = "SZDEntryfoodsKey"
    let locationKey = "SZDEntrylocationKey"
    let poopTypeKey = "SZDEntrypoopTypeKey"
    let tagsKey = "SZDEntrytagsKey"
    let commentsKey = "SZDEntrycommentsKey"
    
    
    override init (entity: NSEntityDescription, insertInto context: NSManagedObjectContext?)
    {
        super.init(entity: entity, insertInto: context)
    }
    
    public override func awakeFromInsert()
    {
        super.awakeFromInsert()
        
        self.startDate = Date()
    }
    
    public func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.endDate, forKey: self.endDateKey)
        aCoder.encode(self.squares, forKey: self.squaresKey)
        aCoder.encode(self.startDate, forKey: self.startDateKey)
        aCoder.encode(self.wipes, forKey: self.wipesKey)
        aCoder.encode(self.characteristics, forKey: self.characteristicsKey)
        aCoder.encode(self.colour, forKey: self.colourKey)
        aCoder.encode(self.foods, forKey: self.foodsKey)
        aCoder.encode(self.location, forKey: self.locationKey)
        aCoder.encode(self.poopType, forKey: self.poopTypeKey)
        aCoder.encode(self.tags, forKey: self.tagsKey)
        aCoder.encode(self.comments, forKey: self.commentsKey)
    }
    
    public required init?(coder aDecoder: NSCoder)
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        guard let entity = NSEntityDescription.entity(forEntityName: "SZDEntry", in: context) else {
            SZDError("NSEntityDescription failed to init.")
            return nil
        }
        
        super.init(entity: entity, insertInto: nil)
        
        guard let startDate = aDecoder.decodeObject(forKey: self.startDateKey) as? Date ,
            let characteristics = aDecoder.decodeObject(forKey: self.characteristicsKey) as? [String : Int16],
            let colour = aDecoder.decodeObject(forKey: self.colourKey) as? SZDColour,
            let poopType = aDecoder.decodeObject(forKey: self.poopTypeKey) as? SZDPoopType,
            let location = aDecoder.decodeObject(forKey: self.locationKey) as? SZDLocation,
            let squares = aDecoder.decodeObject(forKey: self.squaresKey) as? Int16,
            let wipes = aDecoder.decodeObject(forKey: self.wipesKey) as? Int16
            else
        {
            SZDLog("Unable to decode object(s).")
            return nil
        }
        
        let endDate = aDecoder.decodeObject(forKey: self.endDateKey) as? Date
        let foods = aDecoder.decodeObject(forKey: self.foodsKey) as? NSSet
        let tags = aDecoder.decodeObject(forKey: self.tagsKey) as? NSSet
        let comments = aDecoder.decodeObject(forKey: self.commentsKey) as? String
        
        self.endDate = endDate
        self.squares = squares
        self.startDate = startDate
        self.wipes = wipes
        self.characteristics = characteristics
        self.colour = colour
        self.foods = foods
        self.location = location
        self.poopType = poopType
        self.tags = tags
        self.comments = comments 
    }

    
    // an entry has to know what characteristic level they belong to 
    // for every characteristic, they need a characteristic level
    
    /*init(startDate: NSDate, endDate: NSDate?, squares: NSNumber?, wipes: NSNumber?,
         poopType: SZDPoopType?, colour: SZDColour?)
    {
        super.init(entity: entity, insertInto: nil)
        
        self.startDate = startDate
        self.endDate = endDate
        self.squares = squares
        self.wipes = wipes
        self.poopType = poopType
        // add entry to poopType
        self.colour = colour
        
    }*/
}
