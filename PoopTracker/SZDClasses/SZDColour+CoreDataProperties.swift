//
//  SZDColour+CoreDataProperties.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-02-19.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit


extension SZDColour {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SZDColour> {
        return NSFetchRequest<SZDColour>(entityName: "SZDColour");
    }

    @NSManaged public var colour: UIColor
    @NSManaged public var name: String
    @NSManaged public var entries: NSSet?
    @NSManaged public var index: Int16
}

// MARK: Generated accessors for entries
extension SZDColour {

    @objc(addEntriesObject:)
    @NSManaged public func addToEntries(_ value: SZDEntry)

    @objc(removeEntriesObject:)
    @NSManaged public func removeFromEntries(_ value: SZDEntry)

    @objc(addEntries:)
    @NSManaged public func addToEntries(_ values: NSSet)

    @objc(removeEntries:)
    @NSManaged public func removeFromEntries(_ values: NSSet)

}
