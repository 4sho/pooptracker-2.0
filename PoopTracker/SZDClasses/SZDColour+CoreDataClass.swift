//
//  SZDColour+CoreDataClass.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-02-19.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit


public class SZDColour: NSManagedObject, NSCoding
{
    // coding keys 
    let colourKey = "SZDColourColourKey"
    let nameKey = "SZDColourNameKey"
    let entriesKey = "SZDColourEntriesKey"
    let indexKey = "SZDColourIndexKey"
    
    // MARK: Initializers 
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?)
    {
        super.init(entity: entity, insertInto: context)
    }
    
    // MARK: NSCoding protocol
    
    public func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.colour, forKey: self.colourKey)
        aCoder.encode(self.name, forKey: self.nameKey)
        aCoder.encode(self.entries, forKey: self.entriesKey)
        aCoder.encode(self.index, forKey: self.indexKey)
    }
    
    public required init?(coder aDecoder: NSCoder)
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        guard let entity = NSEntityDescription.entity(forEntityName: "SZDColour", in: context) else {
            SZDLog("NSEntityDescription failed to init.")
            return nil
        }
        
        super.init(entity: entity, insertInto: nil)
        
        guard let colour = aDecoder.decodeObject(forKey: self.colourKey) as? UIColor,
            let name = aDecoder.decodeObject(forKey: self.nameKey) as? String,
            let entries = aDecoder.decodeObject(forKey: self.entriesKey) as? NSSet,
            let index = aDecoder.decodeObject(forKey: self.indexKey) as? Int16
        else {
            SZDLog("Unable to decode object(s).")
            return nil
        }
        
       /* guard let name = aDecoder.decodeObject(forKey: self.nameKey) as? String else {
            return nil
        }
        
        guard let entries = aDecoder.decodeObject(forKey: self.entriesKey) as? NSSet {
            return nil
        }*/
        
        self.colour = colour
        self.name = name
        self.entries = entries
        self.index = index
    }
    
    public func update(name: String?)
    {
        if let name = name
        {
            self.name = name
        }
    }
}
