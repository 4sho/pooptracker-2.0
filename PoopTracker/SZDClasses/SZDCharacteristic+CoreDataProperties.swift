//
//  SZDCharacteristic+CoreDataProperties.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-12.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import CoreData
import UIKit


extension SZDCharacteristic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SZDCharacteristic> {
        return NSFetchRequest<SZDCharacteristic>(entityName: "SZDCharacteristic");
    }

    @NSManaged public var desc: String?
    @NSManaged public var icon: UIImage?
    @NSManaged public var name: String
    @NSManaged public var index: Int16 // list index
    @NSManaged public var id: String // non-changing, for entry to reference characteristic in characteristics array
    @NSManaged public var levels: NSOrderedSet // non-optional

}

// MARK: Generated accessors for levels
extension SZDCharacteristic {

    @objc(insertObject:inLevelsAtIndex:)
    @NSManaged public func insertIntoLevels(_ value: SZDCharacteristicLevel, at idx: Int)

    @objc(removeObjectFromLevelsAtIndex:)
    @NSManaged public func removeFromLevels(at idx: Int)

    @objc(insertLevels:atIndexes:)
    @NSManaged public func insertIntoLevels(_ values: [SZDCharacteristicLevel], at indexes: NSIndexSet)

    @objc(removeLevelsAtIndexes:)
    @NSManaged public func removeFromLevels(at indexes: NSIndexSet)

    @objc(replaceObjectInLevelsAtIndex:withObject:)
    @NSManaged public func replaceLevels(at idx: Int, with value: SZDCharacteristicLevel)

    @objc(replaceLevelsAtIndexes:withLevels:)
    @NSManaged public func replaceLevels(at indexes: NSIndexSet, with values: [SZDCharacteristicLevel])

    @objc(addLevelsObject:)
    @NSManaged public func addToLevels(_ value: SZDCharacteristicLevel)

    @objc(removeLevelsObject:)
    @NSManaged public func removeFromLevels(_ value: SZDCharacteristicLevel)

    @objc(addLevels:)
    @NSManaged public func addToLevels(_ values: NSOrderedSet)

    @objc(removeLevels:)
    @NSManaged public func removeFromLevels(_ values: NSOrderedSet)

}
