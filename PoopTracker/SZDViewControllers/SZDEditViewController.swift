//
//  SZDEditViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2016-12-25.
//  Copyright © 2016 sandzapps. All rights reserved.
//

import UIKit
import CoreLocation

class SZDEditViewController: UITableViewController
{
    // TODO: think about what happens when user decides to hide sections
    // OR if they decide to reorder - this needs to become an array.
    enum Section: Int {
        case Date = 0
        case PoopTypeColour
        //case Colour
        case ToiletPaper
        case Characteristics
        case Location
        case Food
        case Tag
        case Comments
        case Count
    }
    
    enum Tag: String {
        case Food = "Food"
        case Tag = "Tag"
    }
    
    let entry: SZDEntry
    var currentLocation: CLLocation?
    
    var isNewEntry: Bool
    
    init(entry: SZDEntry? = nil)
    {
        if let entry = entry
        {
            self.isNewEntry = false
            self.entry = entry
        }
        else
        {
            self.isNewEntry = true
            self.entry = SZDPoopManager.shared.newEntry()
        }
        
        super.init(style: UITableView.Style.grouped)
        
        self.title = "Edit"
        self.tableView.backgroundColor = UIColor(red: 244/255.0, green: 232/255.0, blue: 243/255.0, alpha: 1.0)

        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 45.0
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCurrentLocation(_:)), name: NSNotification.Name.init(rawValue: "CurrentLocation"), object: nil)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        SZDLog("")
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SZDLog("")
        super.viewWillAppear(animated)
        
        self.tableView.reloadData()
        
        // get current location here
        SZDLocationManager.shared.requestCurrentLocation()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        SZDLog("")
        super.viewDidDisappear(animated)
    }
    
    @objc func updateCurrentLocation(_ notification: Notification)
    {
        if let userInfo = notification.userInfo,
            let currentLocation = userInfo["currentLocation"] as? CLLocation
        {
            SZDLog("Current Location: \(currentLocation)")
            
            if let knownLocation = SZDLocationManager.shared.knownLocation(with: currentLocation)
            {
                entry.location = knownLocation
            }
            self.currentLocation = currentLocation
            self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: Section.Location.rawValue)], with: UITableView.RowAnimation.automatic)
        }
    }
    
    @objc func cancel()
    {
        if self.isNewEntry
        {
            SZDPoopManager.shared.remove(entry: self.entry)
        }
        else
        {
            SZDPoopManager.shared.refresh(entry: self.entry, mergeChanges: false)
        }
        
        self.disappear()
    }
    
    @objc func done()
    {
        SZDLog("")
        if let squaresCell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: Section.ToiletPaper.rawValue)) as? EditValueTableViewCell
        {
            self.entry.squares = Int16(squaresCell.value)
        }
        
        if let wipesCell = self.tableView.cellForRow(at: IndexPath.init(row: 1, section: Section.ToiletPaper.rawValue)) as? EditValueTableViewCell
        {
            self.entry.wipes = Int16(wipesCell.value)
        }
        
        if let commentsCell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: Section.Comments.rawValue)) as? EditTextViewTableViewCell
        {
            self.entry.comments = commentsCell.textView.text
        }
        
        do
        {
            try self.entry.managedObjectContext?.save()
        }
        catch let error
        {
            SZDError(error.localizedDescription)
        }
        
        self.disappear()
    }
    
    func disappear()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension SZDEditViewController // UITableViewDataSource
{
    // MARK: Sections
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return Section.Count.rawValue
    }
    
    /*override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView
        {
            headerView.textLabel?.textColor = UIColor.white
        }
    }*/
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        var header: String?
        
        if let section = Section(rawValue: section)
        {
            switch section
            {
            case .Date:
                header = "Date"
            case .PoopTypeColour:
                header = "Type & Colour"
            /*case .Colour:
                header = "Colour"*/
            case .ToiletPaper:
                header = "Toilet Paper"
            case .Characteristics:
                header = "Characteristics"
            case .Location:
                header = "Location"
            case .Food:
                header = "Food"
            case .Tag:
                header = "Tag"
            case .Comments:
                header = "Comments"
            default:
                break
            }
        }
        
        return header
    }

    // MARK: Rows
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == Section.Characteristics.rawValue
        {
            return SZDCharacteristicManager.shared.allCharacteristics.count
        }
        else if section == Section.PoopTypeColour.rawValue
        {
            return 2
        }
        else if section == Section.ToiletPaper.rawValue
        {
            return 2
        }
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let section = indexPath.section
        let row = indexPath.row
        
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "EditTableCell")
        
        // TODO: only those that push view controllers are enabled
        //cell.isUserInteractionEnabled = false
        // TODO: dequeue reuseable
        
        var textLabelText = ""
        var detailLabelText = ""
        
        if let section = Section(rawValue: indexPath.section)
        {
            switch section {
            case .Date:
                textLabelText = String.init(format: "Date: %@", DateFormatter.localizedString(from: entry.startDate, dateStyle: DateFormatter.Style.medium, timeStyle: .short))
            case .Characteristics:
                if let characteristic = SZDCharacteristicManager.shared.characteristic(at: indexPath.row)
                {
                    textLabelText = String(format: "%@: %d", characteristic.name, entry.characteristics[characteristic.id] ?? 1337) // TODO: ???
                }
            case .Location:
                // if none, show current location
                // if has a closest known location, use that.
                if self.entry.location.name == "None"
                {
                    textLabelText = "Location"
                    detailLabelText = "\(self.currentLocation?.coordinate.longitude) \(self.currentLocation?.coordinate.longitude)"
                }
                else
                {
                    textLabelText = self.entry.location.name ?? ""
                    detailLabelText = "\(self.entry.location.location.coordinate.longitude) \(self.entry.location.location.coordinate.longitude)"
                }
            default:
                break
            }
        }
        
        if section == Section.Date.rawValue
        {
            let cell = EditDateTableViewCell(startDate: self.entry.startDate, endDate: self.entry.endDate)
            
            return cell
        }
        else if section == Section.PoopTypeColour.rawValue
        {
            if row == 0 // PoopType
            {
                cell.textLabel?.text = entry.poopType.name
                cell.detailTextLabel?.text = entry.poopType.desc
                cell.imageView?.image = UIImage(color: UIColor.brown, size: CGSize(width: 25, height: 25))
            }
            else // Colour
            {
                cell.textLabel?.text = entry.colour.name
                cell.imageView?.image = UIImage(color: entry.colour.colour, size: CGSize(width: 25, height: 25))
            }
            
            return cell
        }
        else if section == Section.ToiletPaper.rawValue
        {
            if row == 0 // Squares
            {
                let cell = EditValueTableViewCell(value: Int(self.entry.squares))
                cell.textLabel?.text = "Squares"
                return cell
            }
            else // Wipes
            {
                let cell = EditValueTableViewCell(value: Int(self.entry.wipes))
                cell.textLabel?.text = "Wipes"
                return cell
            }
        }
        else if section == Section.Characteristics.rawValue
        {
            let characteristic = SZDCharacteristicManager.shared.characteristic(at: row)!
            let cell = EditCharacteristicTableViewCell(characteristic: characteristic, characteristicSliderDelegate: self)
            let valueOnSlider = cell.slider.valueOnSliderFrom(realValue: Float(entry.characteristics[characteristic.id] ?? 0) / 100.0)
            SZDLog("realvalue: \(Float(entry.characteristics[characteristic.id] ?? 0) / 100.0), valueOnSlider: \(valueOnSlider)")
            cell.slider.setValue(cell.slider.valueOnSliderFrom(realValue: Float(entry.characteristics[characteristic.id] ?? 0) / 100.0), animated: true)
            return cell
        }
        else if section == Section.Food.rawValue
        {
            if let foods = entry.foods
            {
                for case let food as SZDFood in foods
                {
                    if !textLabelText.isEmpty
                    {
                        textLabelText += ", "
                    }
                    
                    textLabelText += food.name
                }
            }
            else
            {
                textLabelText = "No Foods"
            }
        }
        else if section == Section.Tag.rawValue
        {
            if let tags = entry.tags
            {
                for case let tag as SZDTag in tags
                {
                    if !textLabelText.isEmpty
                    {
                        textLabelText += ", "
                    }
                    
                    textLabelText += tag.name
                }
            }
            else
            {
                textLabelText = "No Tags"
            }
        }
        else if section == Section.Comments.rawValue
        {
            let cell = EditTextViewTableViewCell()
            cell.textView.delegate = self
            cell.textView.text = entry.comments ?? ""
            return cell
        }
        
        cell.textLabel?.text = textLabelText
        cell.detailTextLabel?.text = detailLabelText
        
        return cell
    }
}

extension SZDEditViewController // UITableViewDelegate
{
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        SZDLog("\(indexPath)")
        tableView.deselectRow(at: indexPath, animated: true)
        
        let section = indexPath.section
        let row = indexPath.row
        
        // push view controllers
        if indexPath.section == Section.PoopTypeColour.rawValue
        {
            if indexPath.row == 0 // PoopType
            {
                let typeSelectionList = SZDTypeSelectionViewController(entry: self.entry)
                self.navigationController?.pushViewController(typeSelectionList, animated: true)
            }
            else // Colour
            {
                let colourSelectionList = SZDColourSelectViewController(entry: self.entry)
                colourSelectionList.currentMode = .Select
                self.navigationController?.pushViewController(colourSelectionList, animated: true)
            }
        }
        else if indexPath.section == Section.Location.rawValue
        {
            let locationSelectionList = SZDLocationListViewController(entry: self.entry)
            locationSelectionList.currentMode = .Select
            self.navigationController?.pushViewController(locationSelectionList, animated: true)
        }
        else if section == Section.ToiletPaper.rawValue
        {
            if let cell = tableView.cellForRow(at: indexPath)
            {
                if cell.canBecomeFirstResponder && !cell.isFirstResponder
                {
                    SZDLog("becomefirstresponder")
                    tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                    cell.becomeFirstResponder()
                }
            }
        }
        else if section == Section.Date.rawValue
        {
            self.navigationController?.pushViewController(EditDateViewController(delegate: self, startDate: self.entry.startDate, endDate: self.entry.endDate), animated: true)
        }
        else if section == Section.Food.rawValue
        {
            let foodTagViewController = TagViewController()
            foodTagViewController.delegate = self
            foodTagViewController.dataSource = self
            foodTagViewController.title = Tag.Food.rawValue
            
            self.navigationController?.pushViewController(foodTagViewController, animated: true)
        }
        else if section == Section.Tag.rawValue
        {
            let tagViewController = TagViewController()
            tagViewController.delegate = self
            tagViewController.dataSource = self
            tagViewController.title = Tag.Tag.rawValue
            
            self.navigationController?.pushViewController(tagViewController, animated: true)
        }
    }
}

extension SZDEditViewController: TagViewControllerDataSource, TagViewControllerDelegate
{
    // MARK: TagViewControllerDataSource
    
    func numberOfSections(in tagViewController: TagViewController) -> Int
    {
        return 1
    }
    
    func tagViewController(_ tagViewController: TagViewController, numberOfTagsInSection section: Int) -> Int
    {
        guard let title = tagViewController.title else
        {
            return 0
        }
        
        if title == Tag.Food.rawValue
        {
            return SZDFoodManager.shared.allFoods.count
        }
        else // Tag.Tag.rawValue
        {
            return SZDTagManager.shared.allTags.count
        }
    }
    
    func tagViewController(_ tagViewController: TagViewController, cellForTagAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = tagViewController.dequeueReusableTagCellAt(indexPath: indexPath)
        
        if let title = tagViewController.title
        {
            if title == Tag.Food.rawValue
            {
                if let food = SZDFoodManager.shared.food(at: indexPath.row)
                {
                    cell.label.text = food.name
                    if entry.foods?.contains(food) ?? false
                    {
                        cell.isSelected = true
                        tagViewController.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
                    }
                }
            }
            else if title == Tag.Tag.rawValue
            {
                if let tag = SZDTagManager.shared.tag(at: indexPath.row)
                {
                    cell.label.text = tag.name
                    if entry.tags?.contains(tag) ?? false
                    {
                        cell.isSelected = true 
                        tagViewController.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
                    }
                }
            }
        }
        
        return cell
    }
    
    // MARK: TagViewControllerDelegate
    
    func tagViewController(_ tagViewController: TagViewController, didSelectTagAt indexPath: IndexPath)
    {
        guard let title = tagViewController.title else
        {
            return
        }
        
        let index = indexPath.row
        
        if title == Tag.Food.rawValue
        {
            if let food = SZDFoodManager.shared.food(at: index)
            {
                self.entry.addToFoods(food)
            }
        }
        else if title == Tag.Tag.rawValue
        {
            if let tag = SZDTagManager.shared.tag(at: index)
            {
                self.entry.addToTags(tag)
            }
        }
    }
    
    func tagViewController(_ tagViewController: TagViewController, didDeSelectTagAt indexPath: IndexPath)
    {
        guard let title = tagViewController.title else
        {
            return
        }
        
        let index = indexPath.row
        
        if title == Tag.Food.rawValue
        {
            if let food = SZDFoodManager.shared.food(at: index)
            {
                self.entry.removeFromFoods(food)
            }
        }
        else if title == Tag.Tag.rawValue
        {
            if let tag = SZDTagManager.shared.tag(at: index)
            {
                self.entry.removeFromTags(tag)
            }
        }
    }
    
    func tagViewController(_ tagViewController: TagViewController, addTagWith name: String)
    {
        guard let title = tagViewController.title else
        {
            return
        }
        
        if title == Tag.Food.rawValue
        {
            SZDFoodManager.shared.add(name: name)
        }
        else if title == Tag.Tag.rawValue
        {
            SZDTagManager.shared.add(name: name)
        }
    }
    
    func tagViewController(_ tagViewController: TagViewController, removeTagWith name: String) -> Int?
    {
        guard let title = tagViewController.title else
        {
            return nil
        }
        
        if title == Tag.Food.rawValue
        {
            if let index = SZDFoodManager.shared.index(of: name)
            {
                SZDFoodManager.shared.delete(name: name)
                return index
            }
        }
        else if title == Tag.Tag.rawValue
        {
            if let index = SZDTagManager.shared.index(of: name)
            {
                SZDTagManager.shared.delete(name: name)
                return index
            }
        }
        
        return nil
    }
}

extension SZDEditViewController: UITextViewDelegate
{

    // TODO: make it shrink properly
    // Grow and shrink textView
    func textViewDidChange(_ textView: UITextView)
    {
        //let contentSize = textView.sizeThatFits(textView.bounds.size)

        let threshold: CGFloat = 100
        if textView.frame.height < threshold
        {
            textView.isScrollEnabled = false
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
        /*else if contentSize.height < threshold
        {
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            textView.isScrollEnabled = true
        }*/
        else
        {
            textView.isScrollEnabled = true
        }
    }
}

extension SZDEditViewController: CharacteristicSliderDelegate
{
    func characteristicSlider(_ id: String, newValue: Float)
    {
        SZDLog("id \(id) newValue \(newValue)")
        
        self.entry.characteristics[id] = Int16(newValue * 100.0)
        SZDLog("entry: \(entry.characteristics[id] ?? 1337)")
    }
}

extension SZDEditViewController: UIViewControllerDelegate
{
    func handle(data: Dictionary<String, Any>)
    {
        
        if let startDate = data[SZDKeys.startDateKey] as? Date
        {
            self.entry.startDate = startDate
            self.entry.endDate = data[SZDKeys.endDateKey] as? Date
            
            self.tableView.reloadRows(at: [IndexPath(row: 0, section: Section.Date.rawValue)], with: .automatic)
        }
    }
}

