//
//  SZDColourSelectViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-09.
//  Copyright © 2017 sandzapps. All rights reserved.
//
// TEMPORARY UNTIL WE HAVE A BASE SELECT CONTROLLER

import UIKit

class SZDColourSelectViewController: SZDSelectViewController
{
    @objc func addColour()
    {
        self.navigationController?.pushViewController(SZDColourEditViewController(), animated: true)
    }
}

extension SZDColourSelectViewController: SelectListDelegate
{
    func list() -> [AnyObject]
    {
        return SZDColourManager.shared.allColours
    }
    
    func cellFor(rowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "ColourCell")
        
        if let colour = SZDColourManager.shared.colour(at: indexPath.row)
        {
            cell.textLabel?.text = "\(colour.index) \(colour.name)"
        }
        
        return cell
    }
    
    func cellIsSelected(atRow indexPath: IndexPath) -> Bool
    {
        if self.currentMode == .Select
        {
            if let entry = self.entry
            {
                if indexPath.row == SZDColourManager.shared.index(of: entry.colour)
                {
                    return true
                }
            }
        }
        
        return false
    }
    
    func didSelect(rowAt indexPath: IndexPath)
    {
        if self.currentMode == .Select
        {
            if let entry = self.entry
            {
                SZDColourManager.shared.setColourFor(entry: entry, with: indexPath.row)
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        else if self.currentMode == .List
        {
            if let colour = SZDColourManager.shared.colour(at: indexPath.row)
            {
                let colourEditVC = SZDColourEditViewController(colour: colour)
                self.navigationController?.pushViewController(colourEditVC, animated: true)
            }
        }
    }
    
    func rightBarButtonItem() -> UIBarButtonItem?
    {
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addColour))
        
        return barButtonItem
    }
    
}
