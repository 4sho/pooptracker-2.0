//
//  SZDLogViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2016-12-25.
//  Copyright © 2016 sandzapps. All rights reserved.
//

import UIKit

class SZDLogViewController: UITableViewController
{
    // MARK: Initializers
    
    convenience init()
    {
        self.init(style: UITableView.Style.plain)
    }
    
    override init(style: UITableView.Style)
    {
        super.init(style: UITableView.Style.plain)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(pushAddEntryController))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad()
    {
        SZDLog("")
        self.title = "Log"
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        SZDLog("")
        super.viewDidAppear(animated)
        
        self.tableView.reloadData()
    }
    
    // MARK: UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        SZDLog("row: \(indexPath.row)")
        let cell = UITableViewCell.init(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = "!"
        
        let entry = SZDPoopManager.shared.allEntries[indexPath.row]
        
        cell.backgroundColor = entry.colour.colour
        cell.textLabel?.text = String(format: "%@ %@", DateFormatter.localizedString(from: entry.startDate, dateStyle: .medium, timeStyle: .medium), entry.poopType.name ?? "")
        
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return SZDPoopManager.shared.allEntries.count
    }
    
    // MARK: UITableViewDelegate 
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let entry = SZDPoopManager.shared.allEntries[indexPath.row]
        
        SZDLog("Tapped entry: \(entry)")
        
        self.pushEditEntryController(entry: entry)
    }
    
    // MARK: Navigation
    
    func pushEditEntryController(entry: SZDEntry)
    {
        self.navigationController?.pushViewController(SZDEditViewController(entry: entry), animated: true)
    }
    
    @objc func pushAddEntryController()
    {
        self.navigationController?.pushViewController(SZDEditViewController(), animated: true)
    }
    
}
