//
//  SZDSelectViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-09.
//  Copyright © 2017 sandzapps. All rights reserved.
//
/* 
    Goals: Template Select View Controller 
    
    Modes: <- can just be did select
        - Selectable
        - Editable
 
 */

import UIKit

class SZDSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    enum Mode: Int
    {
        case List
        case Select
    }
    
    var currentMode: Mode = Mode.List
    
    //var list: [AnyObject]
    let tableView: UITableView
    var entry: SZDEntry?
    
    var listDelegate: SelectListDelegate?
    
    
    // MARK: Initializers
    
    init()
    {
        SZDLog("")
        
        self.tableView = UITableView.init(frame: UIScreen.main.bounds, style: .plain)
        
        super.init(nibName: nil, bundle: nil)
        
        self.listDelegate = self as? SelectListDelegate
        self.navigationItem.rightBarButtonItem = self.listDelegate?.rightBarButtonItem()
    }
    
    convenience init(entry: SZDEntry)
    {
        self.init()
        
        self.entry = entry
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.addSubview(self.tableView)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tintColor = UIColor.blue
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.listDelegate?.list().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.listDelegate?.cellFor(rowAt: indexPath) ?? self.tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        
        if let selected = self.listDelegate?.cellIsSelected(atRow: indexPath), selected == true
        {
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        }
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.listDelegate?.didSelect(rowAt: indexPath)
    }

}

protocol SelectListDelegate
{
    func cellIsSelected(atRow: IndexPath) -> Bool
    func didSelect(rowAt: IndexPath)
    func list() -> [AnyObject]
    func cellFor(rowAt: IndexPath) -> UITableViewCell
    func rightBarButtonItem() -> UIBarButtonItem?
}

extension SelectListDelegate
{
    // Implement these to get a list that is selectable 
    func cellIsSelected(atRow indexPath: IndexPath) -> Bool
    {
        SZDLog("Using default")
        return false
    }
    
    func didSelect(rowAt indexPath: IndexPath)
    {
        SZDLog("Using default")
        SZDLog("didSelect: \(indexPath)")
    }
    
    func list() -> [AnyObject]
    {
        SZDLog("Using default")
        return Array()
    }
    
    func cellFor(rowAt indexPath: IndexPath) -> UITableViewCell
    {
        SZDLog("Using default")
        let cell = UITableViewCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = "Cell \(indexPath.row)"
    
        return cell
    }
    
    func rightBarButtonItem() -> UIBarButtonItem?
    {
        SZDLog("Using default")
        return nil
    }
    
}
