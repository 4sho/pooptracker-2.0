//
//  SZDLocationEditViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-05.
//  Copyright © 2017 sandzapps. All rights reserved.
//

//  Editable view and non-editable view

import UIKit
import CoreLocation

class SZDLocationEditViewController: UIViewController
{
    let locationLabel = UILabel()
    
    let nameField = UITextField()
    
    var knownLocation: SZDLocation?
    var location: CLLocation?
    
    var locationManager = CLLocationManager()
    
    convenience init(knownLocation: SZDLocation)
    {
        self.init()
        
        self.knownLocation = knownLocation
    }
    
    convenience init(location: CLLocation)
    {
        self.init()
        
        self.location = location
    }
    
    init()
    {
        super.init(nibName: nil, bundle: nil)
        
        self.title = "Edit Location"
        
        self.nameField.placeholder = "Name"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        self.locationLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        self.nameField.frame = CGRect(x: 0, y: 40, width: self.view.frame.width, height: 40)
        
        self.view.addSubview(self.locationLabel)
        self.view.addSubview(self.nameField)
        
        if let knownLocation = self.knownLocation
        {
            self.locationLabel.text = "\(knownLocation.location.coordinate)"
            self.nameField.text = knownLocation.name
        }
        else if let location = self.location
        {
            self.locationLabel.text = "new: \(location.coordinate)"
        }
        else
        {
            // Get location here 
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.distanceFilter = 5
            
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        let name = self.nameField.text
        
        if let knownLocation = self.knownLocation
        {
            SZDLocationManager.shared.update(location: knownLocation, name: name)
        }
        else if let location = self.location
        {
            SZDLocationManager.shared.add(location: location, name: name, desc: nil, image: nil)
        }
    }
    
}

extension SZDLocationEditViewController : CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let currentLocation = locations.last
        {
            self.location = currentLocation
            self.locationLabel.text = "new: \(currentLocation.coordinate)"
            
            self.locationManager.stopUpdatingLocation()
        }
    }
}
