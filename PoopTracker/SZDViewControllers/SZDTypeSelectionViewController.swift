//
//  SZDTypeSelectionViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-06.
//  Copyright © 2017 sandzapps. All rights reserved.
//

/*
 TEMPORARY UNTIL WE HAVE A BASE SELECT CONTROLLER
    Goals:  
        - table view inside
        - for anything. very flexible. Just pass in values?? 
        - if we have an add cell, then how will we associate? maybe pass in the manager?
 
    TODO: figure out a way to not pass entry in, but to return the selected type
 */

import UIKit

class SZDTypeSelectionViewController: SZDSelectViewController
{
    
}

extension SZDTypeSelectionViewController: SelectListDelegate
{
    func list() -> [AnyObject]
    {
        return SZDTypeManager.shared.allPoopTypes
    }
    
    func cellFor(rowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "ColourCell")
        
        if let type = SZDTypeManager.shared.poopType(at: indexPath.row)
        {
            cell.textLabel?.text = type.name
        }
        
        return cell
    }
    
    func cellIsSelected(atRow indexPath: IndexPath) -> Bool {
        if let entry = self.entry
        {
            if indexPath.row == SZDTypeManager.shared.index(of: entry.poopType)
            {
                return true
            }
        }
        
        return false
    }
    
    func didSelect(rowAt indexPath: IndexPath)
    {
        if let entry = self.entry
        {
            SZDTypeManager.shared.setTypeFor(entry: entry, withIndex: indexPath.row)
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
}
