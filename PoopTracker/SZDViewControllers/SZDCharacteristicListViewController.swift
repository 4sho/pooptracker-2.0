//
//  SZDCharacteristicListViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-13.
//  Copyright © 2017 sandzapps. All rights reserved.
//
/* TEMPORARY */

import UIKit

class SZDCharacteristicListViewController: UIViewController
{
    var list: [SZDCharacteristic]
    {
        get
        {
            return SZDCharacteristicManager.shared.allCharacteristics
        }
    }
    let tableView: UITableView

    init()
    {
        self.tableView = UITableView()
        
        super.init(nibName: nil, bundle: nil)
        
        self.title = "Controller"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(pushAddController))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.frame = self.view.frame
        self.view.addSubview(self.tableView)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.reloadData()
    }
    
    @objc func pushAddController()
    {
        self.navigationController?.pushViewController(SZDCharacteristicAddViewController(), animated: true)
    }
}

extension SZDCharacteristicListViewController: UITableViewDataSource, UITableViewDelegate
{
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        SZDLog("\(editingStyle) \(indexPath)")
        if editingStyle == .delete
        {
            SZDCharacteristicManager.shared.delete(self.list[indexPath.row])
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // TODO: dequeue for the rest
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell.init(style: .default, reuseIdentifier: "Cell")
        
        let characteristic = self.list[indexPath.row]
        
        var text = "\(characteristic.name) (\(characteristic.levels.count)) "

        for level in characteristic.levels
        {
            if let level = level as? SZDCharacteristicLevel
            {
                text.append("-\(level.max)")
            }
        }
        
        cell.textLabel?.text = text
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        SZDLog("didSelect: \(indexPath)")
        
        self.navigationController?.pushViewController(SZDCharacteristicAddViewController(characteristic: self.list[indexPath.row]), animated: true)
    }
    
}
