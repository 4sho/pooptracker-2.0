//
//  SZDCharacteristicAddViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-14.
//  Copyright © 2017 sandzapps. All rights reserved.
//
/*
    GOAL: When adding a new characteristic 
    Initialize with 0/none range and 1-100/value range.
 
 For testing: 
    If there's a value for level 1, split it there, if level 2, split it there.
    For a request to make characteristic levels, the ranges must not overlap and must include all values between 1-100. 
 The question is: do we need to be able to acces
 */

import UIKit

class SZDCharacteristicAddViewController: UIViewController
{
    let characteristic: SZDCharacteristic
    
    let nameField: UITextField = UITextField()
    let level1 = UITextField()
    let level2 = UITextField()
    let level3 = UITextField()
    
    init(characteristic: SZDCharacteristic)
    {
        self.characteristic = characteristic
        
        super.init(nibName: nil, bundle: nil)
    }
    
    convenience init()
    {
        SZDCharacteristicManager.shared.add(characteristic: "New", desc: nil, icon: nil, ranges: [0, 100])
        self.init(characteristic: SZDCharacteristicManager.shared.characteristic(at: SZDCharacteristicManager.shared.allCharacteristics.count - 1)!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Edit Characteristic"
        self.view.backgroundColor = UIColor.white
        
        let fieldHeight: CGFloat = 40.0
        
        self.nameField.frame = CGRect(x: 0, y: fieldHeight*0, width: self.view.frame.width, height: fieldHeight)
        self.level1.frame = CGRect(x: 0, y: fieldHeight*1, width: self.view.frame.width, height: fieldHeight)
        self.level2.frame = CGRect(x: 0, y: fieldHeight*2, width: self.view.frame.width, height: fieldHeight)
        self.level3.frame = CGRect(x: 0, y: fieldHeight*3, width: self.view.frame.width, height: fieldHeight)
        
        self.nameField.placeholder = "Name"
        self.level1.placeholder = "Level 1 Max"
        self.level2.placeholder = "Level 2 Max"
        self.level3.placeholder = "Level 3 Max"
        
        self.nameField.text = characteristic.name
        
        self.view.addSubview(self.nameField)
        self.view.addSubview(self.level1)
        self.view.addSubview(self.level2)
        self.view.addSubview(self.level3)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let text = self.nameField.text
        {
            self.characteristic.name = text
        }
        
        if let l1 = Int16(self.level1.text ?? ""), l1 >= 0 && l1 <= 100
        {
            SZDLog("Level 1: \(l1)")
            if let l2 = Int16(self.level2.text ?? ""), l2 > l1 && l2 <= 100
            {
                SZDLog("Level 2: \(l2)")
                if let l3 = Int16(self.level3.text ?? ""), l3 > l1 && l3 <= 100
                {
                    SZDLog("Level 3: \(l3)")
                    
                    self.characteristic.setLevels(ranges: [l1, l2, l3, 100])
                }
                else
                {
                    self.characteristic.setLevels(ranges: [l1, l2, 100])
                }
            }
            else
            {
                self.characteristic.setLevels(ranges: [l1, 100])
            }
        }
    }
    
}
