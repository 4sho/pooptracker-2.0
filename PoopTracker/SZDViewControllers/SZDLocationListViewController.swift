//
//  SZDLocationListViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-05.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

class SZDLocationListViewController: SZDSelectViewController
{
    @objc func addLocation()
    {
        self.navigationController?.pushViewController(SZDLocationEditViewController(), animated: true)
    }
}

extension SZDLocationListViewController: SelectListDelegate
{
    func list() -> [AnyObject]
    {
        return SZDLocationManager.shared.allLocations
    }
    
    func cellFor(rowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "LocationCell")
        
        if let location = SZDLocationManager.shared.knownLocation(at: indexPath.row)
        {
            cell.textLabel?.text = "\(location.name) \(location.location.coordinate.latitude) \(location.location.coordinate.longitude)"
        }
        
        return cell
    }
    
    func cellIsSelected(atRow indexPath: IndexPath) -> Bool
    {
        if self.currentMode == .Select
        {
            if let entry = self.entry
            {
                if indexPath.row == SZDLocationManager.shared.index(of: entry.location)
                {
                    return true
                }
            }
        }
        
        return false
    }
    
    func didSelect(rowAt indexPath: IndexPath)
    {
        if self.currentMode == .Select
        {
            if let entry = self.entry, let location = SZDLocationManager.shared.knownLocation(at: indexPath.row)
            {
                entry.location = location
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        else if self.currentMode == .List
        {
            if let location = SZDLocationManager.shared.knownLocation(at: indexPath.row)
            {
                let locationEditVC = SZDLocationEditViewController(knownLocation: location)
                self.navigationController?.pushViewController(locationEditVC, animated: true)
            }
        }
    }
    
    func rightBarButtonItem() -> UIBarButtonItem?
    {
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addLocation))
        
        return barButtonItem
    }
    
}
