//
//  CharacteristicSlider.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-17.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

// Each slider belongs to a Characteristic cell. Each slider will have an id. when value changed, call delegate with id and value

protocol CharacteristicSliderDelegate: class
{
    func characteristicSlider(_ id: String, newValue: Float)
}

class CharacteristicSlider: UISlider
{
    static let noneFraction: Float = 0.1
    weak var delegate: CharacteristicSliderDelegate?
    
    // Keep track if frame has changed
    var currentFrame: CGRect
    
    var characteristic: SZDCharacteristic
    
    let height: CGFloat = 16
    
    var title: UILabel = UILabel()
    var desc: String?
    var icon: UIImage?
    
    var ticks: [UIImageView] = []
    var labels: [UILabel] = []
    
    init(characteristic: SZDCharacteristic, delegate: CharacteristicSliderDelegate)
    {
        self.characteristic = characteristic
        self.delegate = delegate
        self.currentFrame = CGRect.zero
        
        super.init(frame: CGRect.zero)
        
        //self.backgroundColor = UIColor.lightGray
        
        if let icon = self.characteristic.icon
        {
            self.setThumbImage(icon, for: .normal)
        }
        else
        {
            self.setThumbImage(UIImage.init(color: UIColor.green, size: CGSize.init(width: 10, height: 10)), for: .normal)
        }
        
        // same colour
        self.minimumTrackTintColor = UIColor.black
        self.maximumTrackTintColor = UIColor.black
        
        self.addTarget(self, action: #selector(valueChanged), for: UIControl.Event.touchUpInside)
    }
    
    /*override func draw(_ rect: CGRect)
    {
        super.draw(rect)
        
        SZDLog("\(rect)")
        
        for tick in self.ticks
        {
            tick.removeFromSuperview()
        }
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(2.0)
        context?.setStrokeColor(UIColor.red.cgColor)
        
        //let y = -20
        
        
        for case let level as SZDCharacteristicLevel in self.characteristic.levels
        {
            let x = rect.width / 100.0 * CGFloat(level.min)
            
            context?.move(to: CGPoint(x: x, y: 20))
            context?.addLine(to: CGPoint(x: x, y: -20))
            context?.strokePath()
            
            let tick = UIImageView(image: UIGraphicsGetImageFromCurrentImageContext())
            self.ticks.append(tick)
            self.addSubview(tick)
        }
        
        UIGraphicsEndImageContext()
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*func frameWithoutNone() -> CGRect
    {
        var frame = self.frame
        // shift to new x
        frame.origin.x += frame.size.width * CGFloat(CharacteristicSlider.noneFraction)
        // shrink width
        frame.size.width = frame.size.width * (1 - CGFloat(CharacteristicSlider.noneFraction))
        return frame
    }*/
    
    // realValue = (sliderValue - noneFraction) / (1 - noneFraction)
    // sliderValue = (realValue * (1 - noneFraction)) + noneFraction
    // From data value, translate this to slider location which is the slider value 
    func valueOnSliderFrom(realValue: Float) -> Float
    {
        if realValue == 0
        {
            return 0
        }
        
        return (1 - CharacteristicSlider.noneFraction) * realValue + CharacteristicSlider.noneFraction
    }
    
    // Snap slider value
    func snapValueOf(sliderValue: Float) -> Float
    {
        // snap to none
        if value <= CharacteristicSlider.noneFraction
        {
            return 0
        }
        
        return sliderValue
    }
    
    // Using the slider value, translate back to data value to be saved
    func realValueFrom(sliderValue: Float) -> Float
    {
        let sliderValue = self.snapValueOf(sliderValue: sliderValue)
        
        if sliderValue == 0
        {
            return 0
        }
        
        return (sliderValue - CharacteristicSlider.noneFraction) / (1 - CharacteristicSlider.noneFraction)
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        if self.currentFrame != self.frame
        {
            SZDLog("")
            for tick in self.ticks
            {
                tick.removeFromSuperview()
            }
            
            for label in self.labels
            {
                label.removeFromSuperview()
            }
            
            for i in 0 ..< self.characteristic.levels.count
            {
                if let level = self.characteristic.levels.object(at: i) as? SZDCharacteristicLevel
                {
                    self.addTick(realValue: level.max)
                    
                    if i == 1, level.min != level.max
                    {
                        self.addTick(realValue: level.min)
                    }
                }
            }
            self.currentFrame = self.frame
        }
    }

    func addTick(realValue: Int16)
    {
        // tick
        let tick = UIImageView(image: UIImage(color: UIColor.blue, size: CGSize(width: 1, height: 20)))
        
        let valueOnSlider = self.valueOnSliderFrom(realValue: Float(realValue) / 100.0)
        
        // get center x of thumb
        let trackRect = self.trackRect(forBounds: self.bounds)
        let thumbRect = self.thumbRect(forBounds: self.bounds, trackRect: trackRect, value: valueOnSlider)
        let xPos = thumbRect.midX
        
        tick.frame = CGRect(x: xPos, y: 0, width: 1, height: 20)
        
        self.ticks.append(tick)
        self.insertSubview(tick, at: 0)
        
        // label
        let label = UILabel()
        label.text = realValue == 0 ? "N/A" : "\(realValue)"
        label.font = UIFont.systemFont(ofSize: 12)
        label.adjustsFontSizeToFitWidth = true
        
        let labelHeight: CGFloat = 14.0
        let labelWidth = label.text?.width(withHeight: labelHeight, font: label.font) ?? 0
        let labelX = xPos - labelWidth / 2.0
        label.frame = CGRect(x: labelX, y: -labelHeight, width: labelWidth, height: labelHeight)
        
        self.labels.append(label)
        self.insertSubview(label, at: 0)
    }
    
    @objc func valueChanged()
    {
        let snapValue = self.snapValueOf(sliderValue: self.value)
        SZDLog("Slider: \(self.value) \(snapValue)")
        if snapValue != self.value
        {
            self.setValue(snapValue, animated: false)
        }
        
        let realValue = self.realValueFrom(sliderValue: self.value)
        self.delegate?.characteristicSlider(self.characteristic.id, newValue: realValue)
    }
    
}
