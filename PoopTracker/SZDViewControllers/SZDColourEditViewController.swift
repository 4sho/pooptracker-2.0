//
//  SZDColourEditViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-03-09.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

class SZDColourEditViewController: UIViewController
{
    let rField: UITextField = UITextField()
    let gField: UITextField = UITextField()
    let bField: UITextField = UITextField()
    
    let nameField: UITextField = UITextField()
    
    var colour: SZDColour?
    
    convenience init(colour: SZDColour)
    {
        self.init()
        
        self.colour = colour
    }
    
    init()
    {
        super.init(nibName: nil, bundle: nil)
        
        self.title = "Edit Colour"
        
        self.rField.placeholder = "Red"
        self.gField.placeholder = "Green"
        self.bField.placeholder = "Blue"
        self.nameField.placeholder = "Name"
        
        self.rField.keyboardType = UIKeyboardType.numberPad
        self.gField.keyboardType = UIKeyboardType.numberPad
        self.bField.keyboardType = UIKeyboardType.numberPad
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        let fieldHeight: CGFloat = 40.0
        
        self.rField.frame = CGRect(x: 0, y: fieldHeight * 0, width: self.view.frame.width, height: 40)
        self.gField.frame = CGRect(x: 0, y: fieldHeight * 1, width: self.view.frame.width, height: 40)
        self.bField.frame = CGRect(x: 0, y: fieldHeight * 2, width: self.view.frame.width, height: 40)
        self.nameField.frame = CGRect(x: 0, y: fieldHeight * 3, width: self.view.frame.width, height: 40)
        
        self.view.addSubview(self.rField)
        self.view.addSubview(self.gField)
        self.view.addSubview(self.bField)
        self.view.addSubview(self.nameField)
        
        if let colour = self.colour
        {
            self.nameField.text = colour.name
            
            let colourObject = colour.colour
            
            var r: CGFloat = 0; var g: CGFloat = 0; var b: CGFloat = 0
            
            colourObject.getRed(&r, green: &g, blue: &b, alpha: nil)
            
            self.rField.text = "\(Int(r*255.0))"
            self.gField.text = "\(Int(g*255.0))"
            self.bField.text = "\(Int(b*255.0))"
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        var r: CGFloat = CGFloat(Int(self.rField.text ?? "") ?? 0) / 255.0
        var g: CGFloat = CGFloat(Int(self.gField.text ?? "") ?? 0) / 255.0
        var b: CGFloat = CGFloat(Int(self.bField.text ?? "") ?? 0) / 255.0
        
        let name = self.nameField.text
        
        SZDLog("r: \(r) g: \(g) b: \(b) name: \(name)")
        
        r = r > 1.0 ? 1.0 : (r < 0 ? 0 : r)
        g = g > 1.0 ? 1.0 : (g < 0 ? 0 : g)
        b = b > 1.0 ? 1.0 : (b < 0 ? 0 : b)
        
        let newColourObject = UIColor.init(red: r, green: g, blue: b, alpha: 1.0)
        
        if let colour = self.colour
        {
            SZDColourManager.shared.update(colour: colour, name: name)
        }
        else
        {
            SZDColourManager.shared.add(colour: newColourObject, name: name)
        }
    }
    
}
