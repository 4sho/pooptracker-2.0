//
//  TagViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-05-01.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

class CollectionViewFlowLeftAlignedLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]?
    {
        let attributes = super.layoutAttributesForElements(in: rect)
        
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY
            {
                leftMargin = sectionInset.left
            }
            
            layoutAttribute.frame.origin.x = leftMargin
            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }
        
        return attributes
    }
}

protocol TagViewControllerDataSource: class 
{
    func numberOfSections(in tagViewController: TagViewController) -> Int
    func tagViewController(_ tagViewController: TagViewController, numberOfTagsInSection section: Int) -> Int
    func tagViewController(_ tagViewController: TagViewController, cellForTagAt indexPath: IndexPath) -> UICollectionViewCell
}

protocol TagViewControllerDelegate: class
{
    func tagViewController(_ tagViewController: TagViewController, didSelectTagAt indexPath: IndexPath)
    func tagViewController(_ tagViewController: TagViewController, didDeSelectTagAt indexPath: IndexPath)
    
    func tagViewController(_ tagViewController: TagViewController, addTagWith name: String)
    func tagViewController(_ tagViewController: TagViewController, removeTagWith name: String) -> Int?
}

class TagViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    static let collectionViewReuseIdentifier = "TagCollectionViewCell"
    let collectionView: UICollectionView
    
    weak var dataSource: TagViewControllerDataSource?
    weak var delegate: TagViewControllerDelegate?
    
    init()
    {
        let flowLayout = CollectionViewFlowLeftAlignedLayout()
        flowLayout.estimatedItemSize = CGSize(width: 100, height: TagCollectionViewCell.tagHeight)
        flowLayout.minimumInteritemSpacing = 10
        flowLayout.minimumLineSpacing = 10
        
        self.collectionView = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: flowLayout)
        self.collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.collectionView.backgroundColor = UIColor.white
        self.collectionView.allowsMultipleSelection = true
        self.collectionView.register(TagCollectionViewCell.self, forCellWithReuseIdentifier: TagViewController.collectionViewReuseIdentifier)
        
        super.init(nibName: nil, bundle: nil)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTag))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        self.view.addSubview(self.collectionView)
        
        self.collectionView.frame = self.view.frame
    }
    
    @objc func addTag()
    {
        let alertController = UIAlertController(title: "Add Tag", message: "Enter tag name", preferredStyle: .alert)
        alertController.addTextField(configurationHandler: nil)
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let addAction = UIAlertAction.init(title: "Add", style: .default, handler: {
            alert in
            if let textField = alertController.textFields?.first, let text = textField.text
            {
                let tagCount = self.collectionView(self.collectionView, numberOfItemsInSection: 0)
                self.delegate?.tagViewController(self, addTagWith: text)
                if self.collectionView(self.collectionView, numberOfItemsInSection: 0) == tagCount + 1
                {
                    self.collectionView.insertItems(at: [IndexPath.init(row: tagCount, section: 0)])
                }
            }
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(addAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func removeTag(tag: UILabel)
    {
        if let text = tag.text
        {
            if let index = self.delegate?.tagViewController(self, removeTagWith: text)
            {
                self.collectionView.deleteItems(at: [IndexPath(row: index, section: 0)])
            }
        }
    }
    
    func dequeueReusableTagCellAt(indexPath: IndexPath) -> TagCollectionViewCell
    {
        return collectionView.dequeueReusableCell(withReuseIdentifier: TagViewController.collectionViewReuseIdentifier, for: indexPath) as! TagCollectionViewCell
    }
    
}

extension TagViewController
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return self.dataSource?.numberOfSections(in: self) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.dataSource?.tagViewController(self, numberOfTagsInSection: section) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        return self.dataSource?.tagViewController(self, cellForTagAt: indexPath) ?? self.dequeueReusableTagCellAt(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        self.delegate?.tagViewController(self, didSelectTagAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        self.delegate?.tagViewController(self, didDeSelectTagAt: indexPath)
    }
}
