//
//  EditValueTableViewCell.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-16.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

class EditValueTableViewCell: UITableViewCell, UIPickerViewDataSource, UIPickerViewDelegate
{
    var value: Int
    
    let valuePicker = UIPickerView()
    
    // make it a textfield
    
    init(value: Int)
    {
        self.value = value
        
        super.init(style: .value1, reuseIdentifier: "EditValueTableViewCell")
        
        self.valuePicker.delegate = self
        self.valuePicker.dataSource = self
        
        if value > 0
        {
            self.detailTextLabel?.text = "\(self.value)"
        }
        else
        {
            self.detailTextLabel?.text = "-"
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UIResponder
    
    override var canBecomeFirstResponder: Bool {
        SZDLog("")
        return true
    }
    override var canResignFirstResponder: Bool {
        return true
    }
    override var inputView: UIView? {
        return self.valuePicker
    }
    
    override var inputAccessoryView: UIView?
    {
        return UIToolbar()
    }
    
    override func becomeFirstResponder() -> Bool
    {
        self.valuePicker.selectRow(self.value, inComponent: 0, animated: true)
        return super.becomeFirstResponder()
    }
    
    // MARK: UIPickerView 
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return Int(INT16_MAX)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if (row == 0)
        {
            return "-"
        }
        
        return "\(row)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.value = row
        
        if (row == 0)
        {
            self.detailTextLabel?.text = String(format: "-", row)
        }
        else
        {
            self.detailTextLabel?.text = String(format: "%d", row)
        }
    }
    
}


