//
//  EditDateTableViewCell.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-16.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

class EditDateTableViewCell: UITableViewCell
{
    var startDate: Date
    var endDate: Date?
    
    private let dateLabel = UILabel()
    private let timeLabel = UILabel()
    private let durationLabel = UILabel()
    
    init(startDate: Date, endDate: Date?)
    {
        self.startDate = startDate
        self.endDate = endDate 
        
        super.init(style: .default, reuseIdentifier: "EditDateTableViewCell")
        
        self.dateLabel.text = self.dateString()
        self.timeLabel.text = self.timeString()
        self.durationLabel.text = self.durationString()
        
        //self.backgroundColor = UIColor.gray
        
        self.dateLabel.font = UIFont.systemFont(ofSize: 14)
        self.durationLabel.font = UIFont.systemFont(ofSize: 14)
        
        //self.timeLabel.textAlignment = .center
        self.timeLabel.font = UIFont.systemFont(ofSize: 20)
        
        let views: [String : UILabel] = [ "dateLabel" : self.dateLabel,
                                          "timeLabel" : self.timeLabel,
                                          "durationLabel" : self.durationLabel ]
        
        for (_, view) in views
        {
            view.translatesAutoresizingMaskIntoConstraints = false
            view.adjustsFontSizeToFitWidth = true
            view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            
            self.contentView.addSubview(view)
        }
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[dateLabel(==16)]-8-[timeLabel]-8-|" , options: [], metrics: nil, views: views))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[dateLabel]-8-[durationLabel]-8-|" , options: [.alignAllBottom, .alignAllTop], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[timeLabel]-8-|" , options: [], metrics: nil, views: views))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
    }
    
    func durationString() -> String?
    {
        return String.prettyString(self.endDate?.timeIntervalSince(self.startDate))
    }
    
    func dateString() -> String
    {
        let string = DateFormatter.localizedString(from: self.startDate, dateStyle: .full, timeStyle: .none)
        
        return string
    }
    
    func timeString() -> String
    {
        var string = ""
        
        if let endDate = self.endDate
        {
            // 4:23 PM - 5:12 PM
            string += DateFormatter.localizedString(from: self.startDate, dateStyle: .none, timeStyle: .short)
            string += " - "
            string += DateFormatter.localizedString(from: endDate, dateStyle: .none, timeStyle: .short)
        }
        else
        {
            string = DateFormatter.localizedString(from: self.startDate, dateStyle: .none, timeStyle: .short)
        }
        
        return string
    }
    
}
