//
//  EditCharacteristicTableViewCell.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-17.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

class EditCharacteristicTableViewCell: UITableViewCell
{
    weak var characteristicSliderDelegate: CharacteristicSliderDelegate?
    var height: CGFloat = 60
    let slider: CharacteristicSlider
    let titleLabel = UILabel()
    
    init(characteristic: SZDCharacteristic, characteristicSliderDelegate: CharacteristicSliderDelegate)
    {
        self.slider = CharacteristicSlider(characteristic: characteristic, delegate: characteristicSliderDelegate)
        self.characteristicSliderDelegate = characteristicSliderDelegate
        
        super.init(style: .value1, reuseIdentifier: "EditValueTableViewCell")
        
        self.selectionStyle = .none
        //self.contentView.backgroundColor = UIColor.yellow
        //self.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.slider)
        
        self.titleLabel.font = UIFont.systemFont(ofSize: 12)
        self.titleLabel.adjustsFontSizeToFitWidth = true
        
        self.titleLabel.text = characteristic.name
                
        //self.titleLabel.frame = self.frame
        //self.slider.frame = self.frame
        
        let views: [String : UIView] = [ "titleLabel" : self.titleLabel,
                                         "slider" : self.slider ]
        
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.slider.translatesAutoresizingMaskIntoConstraints = false
        //self.titleLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //self.slider.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        
        /*self.contentView.addConstraint(NSLayoutConstraint.init(item: self.titleLabel, attribute: .trailing, relatedBy: .equal, toItem: self.slider, attribute: .leading, multiplier: 1.0, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint.init(item: self.titleLabel, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1.0, constant: 8))
        self.contentView.addConstraint(NSLayoutConstraint.init(item: self.slider, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1.0, constant: -8))*/
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[titleLabel]-24-[slider]-8-|", options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[slider]-20-|", options: [], metrics: nil, views: views))
        
        //self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[titleLabel]-8-[slider]-8-|", options: .alignAllCenterY, metrics: nil, views: views))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
