//
//  EditDateFieldTableViewCell.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-20.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import Foundation
import UIKit

class EditDateFieldTableViewCell: UITableViewCell
{
    static let reuseIdentifier = "EditDateFieldTableViewCell"
    let nullDate = "-"

    var datePickerMode: UIDatePicker.Mode
    private var date: Date?
    private var duration: TimeInterval?
    
    let datePicker: UIDatePicker
    
    init(datePicker: UIDatePicker)
    {
        self.datePicker = datePicker
        self.datePickerMode = .dateAndTime
        
        super.init(style: .value1, reuseIdentifier: EditDateFieldTableViewCell.reuseIdentifier)
        
        self.datePicker.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UIResponder 
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override var canResignFirstResponder: Bool {
        return true
    }
    
    override var inputView: UIView? {
        self.datePicker.datePickerMode = self.datePickerMode
        return self.datePicker
    }
    
    override func becomeFirstResponder() -> Bool
    {
        if self.datePickerMode == .dateAndTime
        {
            self.datePicker.date = self.date ?? Date()
        }
        else if self.datePickerMode == .countDownTimer
        {
            self.datePicker.countDownDuration = self.duration ?? 0
        }
        return super.becomeFirstResponder()
    }
    
    func update(with date: Date?)
    {
        self.date = date
        self.detailTextLabel?.text = self.stringFor(self.date)
    }
    
    func update(with duration: TimeInterval?)
    {
        self.duration = duration
        self.detailTextLabel?.text = self.stringFor(self.duration)
    }
    
    @objc func valueChanged()
    {
        if self.isFirstResponder
        {
            if self.datePicker.datePickerMode == .dateAndTime
            {
                self.update(with: self.datePicker.date)
            }
            else if self.datePicker.datePickerMode == .countDownTimer
            {
                self.update(with: self.datePicker.countDownDuration)
            }
        }
    }
    
    func stringFor(_ date: Date?) -> String
    {
        guard let date = date else
        {
            return nullDate
        }
        
        return DateFormatter.localizedString(from: date, dateStyle: .medium, timeStyle: .short)
    }
    
    func stringFor(_ duration: TimeInterval?) -> String
    {
        guard let string = String.prettyString(duration) else
        {
            return nullDate
        }
        
        return string
    }
    
}
