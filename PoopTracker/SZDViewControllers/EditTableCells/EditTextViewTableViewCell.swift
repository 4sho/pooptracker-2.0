//
//  EditTextViewTableViewCell.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-18.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

class EditTextViewTableViewCell: UITableViewCell, UITextViewDelegate
{
    let textView = UITextView()
    
    init()
    {
        super.init(style: .default, reuseIdentifier: "EditTextFieldTableViewCell")
        
        self.selectionStyle = .none
        
        self.textView.delegate = self
        
        //self.textView.backgroundColor = UIColor.lightGray
        self.textView.layer.cornerRadius = 5.0
        self.textView.layer.borderWidth = 1.0
        self.textView.layer.borderColor = UIColor.lightGray.cgColor
        self.textView.font = UIFont.systemFont(ofSize: 14)
        self.textView.isScrollEnabled = false
        
        self.textView.frame = self.contentView.frame
        //self.textField.autoresizingMask = [.flexibleHeight]
        self.textView.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(self.textView)
        
        let views = [ "textField" : self.textView ]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-12-[textField]-12-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[textField]-16-|", options: [], metrics: nil, views: views))
        
        //self.textView.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
