//
//  TagCollectionViewCell.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-05-01.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell
{
    static let tagHeight: CGFloat = 32.0
    static let tagFontSize: CGFloat = 16.0
    static let padding: CGFloat = 20.0
    static let cornerRadius: CGFloat = 16.0
    
    let label: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: TagCollectionViewCell.tagFontSize)
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.layer.cornerRadius = TagCollectionViewCell.cornerRadius
        label.layer.borderColor = UIColor.lightGray.cgColor
        label.layer.borderWidth = 1.0
        return label
    }()
    
    override var isSelected: Bool {
        didSet {
            self.label.backgroundColor = isSelected ? UIColor.lightGray : UIColor.white
            self.label.textColor = isSelected ? UIColor.black : UIColor.lightGray
        }
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        self.addSubview(self.label)
        
        let views = [ "label" : self.label ]
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[label]|", options: .alignAllCenterY, metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[label]|", options: .alignAllCenterX, metrics: nil, views: views))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setText(_ text: String)
    {
        self.label.text = text
    }
    
    func getFrame() -> CGRect
    {
        let width = self.label.text?.width(withHeight: TagCollectionViewCell.tagFontSize, font: self.label.font) ?? -TagCollectionViewCell.padding
        let height = TagCollectionViewCell.tagHeight
        
        return CGRect(x: 0, y: 0, width: width + TagCollectionViewCell.padding, height: height)
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes
    {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        layoutAttributes.frame = self.getFrame()
        
        return layoutAttributes
    }
    
}
