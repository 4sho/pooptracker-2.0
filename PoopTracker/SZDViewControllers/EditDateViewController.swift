//
//  EditDateViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-19.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

protocol UIViewControllerDelegate: class
{
    func handle(data: Dictionary<String, Any>)
}

class EditDateViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    weak var delegate: UIViewControllerDelegate?
    
    enum DateField: Int
    {
        case StartDate = 0
        case EndDate
        case Duration
    }
    
    let startDateTitle = "Start Date"
    let endDateTitle = "End Date"
    let durationTitle = "Duration"
    
    let reuseIdentifier = "EditDateCell"
    let nullDate = "-"
    
    var startDate: Date
    var endDate: Date?
    var duration: TimeInterval?
    
    var tableView: UITableView
    
    let datePicker = UIDatePicker()
    
    init(delegate: UIViewControllerDelegate?, startDate: Date, endDate: Date?)
    {
        self.delegate = delegate
        self.startDate = startDate
        self.endDate = endDate
        self.duration = self.endDate?.timeIntervalSince(self.startDate)
        
        self.tableView = UITableView(frame: UIScreen.main.bounds, style: .grouped)
        
        super.init(nibName: nil, bundle: nil)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        self.view.addSubview(self.tableView)
        self.tableView.backgroundColor = UIColor.white
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        var data: [String : Any] = [ SZDKeys.startDateKey : self.startDate ]
        if let endDate = self.endDate
        {
            data[SZDKeys.endDateKey] = endDate
        }
        self.delegate?.handle(data: data)
    }
    
    func duration(from startDate: Date, to endDate: Date?) -> TimeInterval?
    {
        guard let endDate = endDate else
        {
            return nil
        }
        
        // endDate - startDate
        return endDate.timeIntervalSince(self.startDate)
    }
    
    func endDate(from startDate: Date, duration: TimeInterval?) -> Date?
    {
        guard let duration = duration else
        {
            return nil
        }
        
        // startDate + duration
        return startDate.addingTimeInterval(duration)
    }

}

extension EditDateViewController
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: EditDateFieldTableViewCell.reuseIdentifier) as? EditDateFieldTableViewCell ?? EditDateFieldTableViewCell(datePicker: self.datePicker)
        
        let row = indexPath.row
        if row == 0
        {
            cell.textLabel?.text = "Start Date"
            cell.update(with: self.startDate)
            cell.datePicker.addTarget(self, action: #selector(pickerValueChanged(_:)), for: .valueChanged)
            //cell.datePicker.addTarget(self, action: #selector(startDateChanged(_:), for: .valueChanged)
        }
        else if row == 1
        {
            cell.textLabel?.text = "End Date"
            cell.update(with: self.endDate)
        }
        else if row == 2
        {
            cell.textLabel?.text = "Duration"
            //cell.detailTextLabel?.text = "9m"
            cell.datePickerMode = .countDownTimer
            cell.update(with: self.duration)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let cell = tableView.cellForRow(at: indexPath)
        {
            if cell.canBecomeFirstResponder && !cell.isFirstResponder
            {
                cell.becomeFirstResponder()
            }
        }
    }
    
    @objc func pickerValueChanged(_ datePicker: UIDatePicker)
    {
        let responder = self.view.findFirstResponder()
        
        if responder == self.tableView.cellForRow(at: IndexPath(row: 0, section: 0))
        {
            SZDLog("start")
            self.startDate = datePicker.date
            self.duration = self.duration(from: self.startDate, to: self.endDate)
            
            // affects duration
            if let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? EditDateFieldTableViewCell
            {
                cell.update(with: self.duration)
            }
        }
        else if responder == self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))
        {
            SZDLog("end")
            self.endDate = datePicker.date
            self.duration = self.duration(from: self.startDate, to: self.endDate)
            
            // affects duration
            if let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? EditDateFieldTableViewCell
            {
                cell.update(with: self.duration)
            }
        }
        else if responder == self.tableView.cellForRow(at: IndexPath(row: 2, section: 0))
        {
            SZDLog("old duration: \(self.duration) \(self.endDate)")
            self.duration = datePicker.countDownDuration
            // affects end date
            self.endDate = self.endDate(from: self.startDate, duration: datePicker.countDownDuration)
            SZDLog("new duration: \(self.duration) \(self.endDate)")
            
            if let cell = self.tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? EditDateFieldTableViewCell
            {
                cell.update(with: self.endDate)
            }
        }
    }
    
}
