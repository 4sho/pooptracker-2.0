//
//  SZDKeys.swift
//  PoopTracker
//
//  Created by Sandy House on 2017-04-21.
//  Copyright © 2017 sandzapps. All rights reserved.
//

import UIKit

class SZDKeys
{
    // MARK: Data Communication 
    
    // EditDateViewController 
    static let startDateKey = "startDateKey"
    static let endDateKey = "endDateKey"
}
