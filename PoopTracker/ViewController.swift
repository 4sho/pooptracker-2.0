//
//  ViewController.swift
//  PoopTracker
//
//  Created by Sandy House on 2016-12-25.
//  Copyright © 2016 sandzapps. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    enum Section: Int
    {
        case Features = 0
        case PoopAttributeEditors
        case Test
        case Count
    }
    
    enum Controller: Int
    {
        case Log = 0
    }
    
    var tableView: UITableView
    
    // TMP: 
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    
    // MARK: Initializers
    
    init()
    {

        self.tableView = UITableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        
        super.init(nibName: nil, bundle: nil)
        
        self.tableView.frame = self.view.frame
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        self.tableView = UITableView.init(frame: CGRect.zero, style: UITableView.Style.plain)
        
        super.init(coder: aDecoder)
    }
    
    // MARK: Lifecycle 
    
    override func viewDidLoad()
    {
        SZDLog("")
        super.viewDidLoad()
        
        self.title = "PoopTracker"
        self.view.addSubview(tableView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tableView.reloadData()
    }

    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return Section.Count.rawValue
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if let sectionType = Section(rawValue: section)
        {
            switch sectionType {
            case .Features:
                return "Features"
            case .PoopAttributeEditors:
                return "Poop Attribute Editors"
            case .Test:
                return "Test"
            default:
                break
            }
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if let sectionType = Section(rawValue: section)
        {
            switch sectionType {
            case .Features:
                return 1
            case .PoopAttributeEditors:
                return 3
            case .Test:
                return 1
            default:
                break
            }
        }
        
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell.init(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "Cell")
        
        var text: String = ""
        
        if indexPath.section == Section.Features.rawValue
        {
            if let controllerType = Controller(rawValue: indexPath.row)
            {
                switch controllerType
                {
                case .Log:
                    text = "Log \(SZDPoopManager.shared.allEntries.count)"
                default:
                    break
                }
            }
        }
        else if indexPath.section == Section.PoopAttributeEditors.rawValue
        {
            switch indexPath.row
            {
            case 0:
                text = "Colour Editor \(SZDColourManager.shared.allColours.count)"
            case 1:
                text = "Characteristic Editor \(SZDCharacteristicManager.shared.allCharacteristics.count)"
            case 2:
                text = "Location Editor \(SZDLocationManager.shared.allLocations.count)"
            default:
                break
            }
        }
        else if indexPath.section == Section.Test.rawValue
        {
            text = "Location"
            if let location = self.currentLocation
            {
                text = desc(of: location)
            }
        }
        
        cell.textLabel?.text = text
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // push view controller
        
        var controller: UIViewController?
        
        if indexPath.section == Section.Features.rawValue
        {
            if let controllerType = Controller(rawValue: indexPath.row)
            {
                switch controllerType
                {
                case .Log:
                    controller = SZDLogViewController()
                default:
                    break
                }
            }
        }
        else if indexPath.section == Section.PoopAttributeEditors.rawValue
        {
            switch indexPath.row
            {
            case 0:
                let colourController = SZDColourSelectViewController()
                colourController.currentMode = SZDColourSelectViewController.Mode.List
                
                controller = colourController
            case 1:
                controller = SZDCharacteristicListViewController()
            case 2:
                controller = SZDLocationListViewController()
            default:
                break 
            }
        }
        else if indexPath.section == Section.Test.rawValue
        {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.distanceFilter = 5 // meters
            
            self.locationManager.requestWhenInUseAuthorization()
            
            
            self.locationManager.startUpdatingLocation()
        }

        if let controller = controller
        {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}

// Testing location
extension ViewController: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let currentLocation = locations.last
        {
            self.currentLocation = currentLocation
            //SZDLog("Location: \(currentLocation)")
            let coordinate = currentLocation.coordinate
            let altitude = currentLocation.altitude
            let floor = currentLocation.floor
            let horizontalAccuracy = currentLocation.horizontalAccuracy
            let verticalAccuracy = currentLocation.verticalAccuracy
            let timestamp = currentLocation.timestamp
            
            SZDLog("Location: \n\t coordinate: \(coordinate) \n\t altitude: \(altitude) \n\t floor: \(floor) \n\t horizontalAccuracy: \(horizontalAccuracy) \n\t verticalAccuracy: \(verticalAccuracy) \n\t timestamp: \(timestamp)")
            
            self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: Section.Test.rawValue)], with: .automatic)
            
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func desc(of location: CLLocation) -> String
    {
        let coordinate = location.coordinate
        let altitude = location.altitude
        let floor = location.floor
        let horizontalAccuracy = location.horizontalAccuracy
        let verticalAccuracy = location.verticalAccuracy
        //let timestamp = location.timestamp
        
        let speed = location.speed
        let course = location.course
        
        
        return "\(coordinate.latitude) \(coordinate.longitude) a: \(altitude) f: \(floor) hA: \(horizontalAccuracy) vA: \(verticalAccuracy)|sp: \(speed) crse: \(course)"
    }
    
}
